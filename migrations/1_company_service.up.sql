-- * this table to store companies
create table if not exists "companies" (
    "id" serial primary key,
    "image_url" text not null,
    "company_name" varchar not null,
    "email" varchar(100) not null,
    "phone_number" varchar(50),
    "country" varchar not null,
    "region_state" varchar not null,
    "workers_count" varchar(10) not null,
    "description" text not null,
    "posted" int default 0,
    "created_at" timestamp with time zone default current_timestamp,
    "updated_at" timestamp with time zone
);


-- * this table to store vacancies
create table if not exists "vacancies" (
    "id" serial primary key,
    "company_id" int not null REFERENCES companies(id) ON DELETE CASCADE,
    "title" varchar not null,
    "deadline" varchar(15) not null,
    "employment_form" varchar(20) not null check ("employment_form" in ('remote', 'in-office', 'hybrid')),
    "employment_type" varchar(50) not null check ("employment_type" in ('full-time', 'part-time', 'freelance', 'contract', 'internship', 'apprenticeship', 'seasonal')),
    "country" varchar(100) not null,
    "city" varchar(100) not null,
    "salary_min" numeric(18, 2) not null check ("salary_min" > 0),
    "salary_max" numeric(18, 2) not null check ("salary_max" > 0),
    "currency" varchar(10) not null check ("currency" in ('usd', 'eur', 'rub', 'gbp', 'inr', 'kzt', 'uzs')),
    "salary_period" varchar(20) not null check ("salary_period" in ('month', 'hour', 'year')),
    "education" varchar(30) check ("education" in ('high-school-or-equivalent', 'certification', 'vocational', 'associate', 'bachelor', 'master', 'doctorate')),
    "experience" int not null check ("experience" >= 0),
    "description" text not null,
    "is_active" boolean default false,
    "views_count" int default 0 check ("views_count" >= 0),
    "created_at" timestamp with time zone default current_timestamp,
    "updated_at" timestamp with time zone
);

-- * companies social media table (If have)
create table if not exists "social_medias" (
    "id" serial primary key,
    "company_id" int not null REFERENCES companies(id) ON DELETE CASCADE,
    "name" varchar(50) not null  check ("name" in ('own-website', 'linkedin', 'facebook', 'github', 'instagram', 'telegram', 'youtube')),
    "url" text not null
);

-- * this table to store vacancy  language (Required) one more than
create table if not exists "languages" (
    "id" serial primary key,
    "vacancy_id" int not null REFERENCES vacancies(id) ON DELETE CASCADE,
    "language" varchar(20) not null,
    "level" varchar(30) not null check ("level" in ('a1', 'a2', 'b1', 'b2', 'c1', 'c2', 'n'))
);

-- * this table to store company skills (Required) one more than for vacancy table 
create table if not exists "skills" (
    "id" serial primary key,
    "vacancy_id" int not null REFERENCES vacancies(id) ON DELETE CASCADE,
    "name" varchar not null
);

-- * this table to store markets (yo'nalishlar) in for vacancy table
create table if not exists "markets" (
    "id" serial primary key,
    "vacancy_id" int not null REFERENCES vacancies(id) ON DELETE CASCADE,
    "market" varchar(100) not null
);

create table if not exists "applications" (
    "id" serial primary key,
    "vacancy_id" int not null REFERENCES vacancies(id) ON DELETE CASCADE,
    "applicant_id" int not null check ("applicant_id" > 0),
    --  * status should be default applied in the code while creating application not here!
    "status" varchar(15) not null check ("status" in ('applied', 'screening', 'interview', 'accepted', 'rejected')),
    "rejection_reason" text,
    "created_at" timestamp with time zone default current_timestamp,
    unique(applicant_id, vacancy_id)
);

create table if not exists "blocked_applicants" (
    "id" serial primary key,
    "company_id" int not null REFERENCES companies(id) on DELETE CASCADE,
    "applicant_id" int not null check ("applicant_id" > 0),
    "created_at" timestamp default current_timestamp
);

create table if not exists "company_members" (
    "id" serial primary key,
    "company_id" int not null REFERENCES companies(id) ON DELETE CASCADE,
    "member_id" int not null check ("member_id" > 0),
    "user_type" varchar(30) not null check ("user_type" in ('owner', 'admin')),
    "created_at" timestamp default current_timestamp,
    unique(company_id, member_id)
);