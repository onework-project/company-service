package main

import (
	"fmt"
	"net"

	"gitlab.com/onework-project/company-service/config"
	pb "gitlab.com/onework-project/company-service/genproto/company_service"
	grpcPkg "gitlab.com/onework-project/company-service/pkg/grpc_client"
	"gitlab.com/onework-project/company-service/pkg/logger"
	"gitlab.com/onework-project/company-service/service"
	"gitlab.com/onework-project/company-service/storage"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load(".")

	logger.Init()
	log := logger.GetLogger()

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)

	if err != nil {
		log.Fatalf("failed to connect to database: %v", err)
	}

	strg := storage.NewStoragePg(psqlConn)

	grpcConn, err := grpcPkg.New(cfg)
	if err != nil {
		log.Fatalf("failed to get grpc connections: %v", err)
	}

	companyService := service.NewCompanyService(strg, &log)
	companyMemberService := service.NewCompanyMemberService(strg, &log, grpcConn)
	vacancyService := service.NewVacancyService(strg, &log)
	applicationService := service.NewApplicationService(strg, &log, grpcConn)
	marketService := service.NewMarketService(strg, &log)
	skillService := service.NewSkillService(strg, &log)
	socialMediaService := service.NewSMService(strg, &log)
	languageService := service.NewLanguageService(strg, &log)
	blockedApplService := service.NewBlockedApplService(strg, &log, grpcConn)

	listen, err := net.Listen("tcp", cfg.GrpcPort)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()

	pb.RegisterCompanyServiceServer(s, companyService)
	pb.RegisterCompanyMemberServiceServer(s, companyMemberService)
	pb.RegisterVacancyServiceServer(s, vacancyService)
	pb.RegisterApplicationServiceServer(s, applicationService)
	pb.RegisterMarketServiceServer(s, marketService)
	pb.RegisterComSkillServiceServer(s, skillService)
	pb.RegisterSMServiceServer(s, socialMediaService)
	pb.RegisterCLanguageServiceServer(s, languageService)
	pb.RegisterBlockedApplicantServiceServer(s, blockedApplService)
	reflection.Register(s)

	log.Info("gRPC server started port in: ", cfg.GrpcPort)
	if s.Serve(listen); err != nil {
		log.Fatalf("Error while listening: %v", err)
	}
}
