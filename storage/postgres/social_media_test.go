package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/company-service/storage/repo"
)

func createSM(t *testing.T) *repo.SocialMedia {
	company := createCompany(t)
	arg := &repo.SocialMedia{
		CompanyID: company.ID,
		Name:      "own-website",
		Url:       faker.URL(),
	}
	sm, err := dbManager.SocialMedia().Create(arg)

	require.NoError(t, err)
	require.Equal(t, arg.CompanyID, sm.CompanyID)
	require.Equal(t, arg.Name, sm.Name)
	require.Equal(t, arg.Url, sm.Url)

	return sm
}

func deleteSM(t *testing.T, CompanyID int64) {
	err := dbManager.SocialMedia().Delete(CompanyID)
	require.NoError(t, err)
}

func TestCraeteSM(t *testing.T) {
	sm := createSM(t)
	deleteSM(t, sm.CompanyID)
}

func TestGetAllSMs(t *testing.T) {
	c := createCompany(t)
	for i := 0; i < 10; i++ {
		_, err := dbManager.SocialMedia().Create(&repo.SocialMedia{
			CompanyID: c.ID,
			Name:      "telegram",
			Url:       faker.URL(),
		})
		require.NoError(t, err)
	}
	resp, err := dbManager.SocialMedia().GetAllSMs(&repo.GetAllSMsParams{
		Limit:     10,
		Page:      1,
		CompanyID: c.ID,
	})
	count := 0
	for i := 0; i < len(resp.SMs); i++ {
		count++
	}
	require.GreaterOrEqual(t, count, 10)

	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteSM(t, c.ID)
	}
}
