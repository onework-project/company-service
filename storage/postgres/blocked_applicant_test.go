package postgres_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/company-service/storage/repo"
)

func createBlockedAppl(t *testing.T) *repo.BlockedApplicant {
	arg := &repo.BlockedApplicant{
		ApplicantID: 1,
	}
	s, err := dbManager.BlockedApplicant().Create(arg)

	require.NoError(t, err)
	require.Equal(t, arg.ApplicantID, s.ApplicantID)

	return s
}

func deleteBlockedAppl(t *testing.T, id, applicantID int64) {
	err := dbManager.BlockedApplicant().Delete(id, applicantID)
	require.NoError(t, err)
}

func TestCraeteBlockedAppl(t *testing.T) {
	s := createBlockedAppl(t)
	deleteBlockedAppl(t, s.ID, s.ApplicantID)
}

func TestGetAllBlockedAppl(t *testing.T) {
	ids := []int64{}
	var applID int64
	for i := 0; i < 10; i++ {
		ap, _ := dbManager.BlockedApplicant().Create(&repo.BlockedApplicant{
			ApplicantID: 1,
		})
		ids = append(ids, ap.ID)
		applID = ap.ApplicantID
	}
	resp, err := dbManager.BlockedApplicant().GetAll(&repo.GetAllBlockedApplsParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(resp.BlockedAppls), 10)

	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteBlockedAppl(t, ids[i], applID)
	}
}
