package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/company-service/storage/repo"
)

type languageRepo struct {
	db *sqlx.DB
}

func NewLanguage(db *sqlx.DB) repo.LanguageStorageI {
	return &languageRepo{
		db: db,
	}
}

func (lr *languageRepo) Create(l *repo.Language) (*repo.Language, error) {
	query := `
		INSERT INTO languages (
			vacancy_id,
			language,
			level
		) VALUES($1, $2, $3)
		RETURNING id
	`

	err := lr.db.QueryRow(
		query,
		l.VacancyID,
		l.Language,
		l.Level,
	).Scan(
		&l.ID,
	)
	if err != nil {
		return nil, err
	}

	return l, nil
}

func (lr *languageRepo) Delete(vacancyID int64) error {
	query := "DELETE FROM languages WHERE vacancy_id=$1"

	result, err := lr.db.Exec(query, vacancyID)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (lr *languageRepo) GetAll(params *repo.GetAllLanguagesParams) (*repo.GetAllLanguagesResult, error) {
	res := repo.GetAllLanguagesResult{
		Languages: make([]*repo.Language, 0),
	}

	offset := (params.Page - 1) * params.Limit
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)
	filter := " WHERE true "
	if params.VacancyID > 0 {
		filter += fmt.Sprintf(` AND vacancy_id=%d `, params.VacancyID)
	}

	query := `
		SELECT 
			id,
			vacancy_id,
			language,
			level
		FROM languages
		` + filter + limit

	rows, err := lr.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			l        repo.Language
			language sql.NullString
			level    sql.NullString
		)
		err := rows.Scan(
			&l.ID,
			&l.VacancyID,
			&language,
			&level,
		)
		if err != nil {
			return nil, err
		}
		l.Language = language.String
		l.Level = level.String

		res.Languages = append(res.Languages, &l)
	}

	queryCount := `SELECT count(1) FROM languages ` + filter
	err = lr.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return &res, nil
}
