package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/company-service/storage/repo"
)

const (
	StatusApplied  = "applied"
	StatusRejected = "rejected"
)

type applicationRepo struct {
	db *sqlx.DB
}

func NewApplicant(db *sqlx.DB) repo.ApplicationStorageI {
	return &applicationRepo{
		db: db,
	}
}

func (ar *applicationRepo) Create(a *repo.Application) (*repo.Application, error) {

	query := `
		INSERT INTO applications (
			vacancy_id,
			applicant_id,
			status
		) VALUES($1, $2, $3)
		RETURNING id, vacancy_id, status, created_at
	`

	err := ar.db.QueryRow(
		query,
		a.VacancyID,
		a.ApplicantID,
		StatusApplied,
	).Scan(
		&a.ID,
		&a.VacancyID,
		&a.Status,
		&a.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return a, nil
}

func (ar *applicationRepo) Get(id int64) (*repo.Application, error) {
	var (
		res    repo.Application
		reason sql.NullString
	)
	query := `
		SELECT 
			id,
			vacancy_id,
			applicant_id,
			status,
			rejection_reason,
			created_at
		FROM applications	
		WHERE id=$1
	`

	err := ar.db.QueryRow(query, id).Scan(
		&res.ID,
		&res.VacancyID,
		&res.ApplicantID,
		&res.Status,
		&reason,
		&res.CreatedAt,
	)
	if err != nil {
		return nil, err
	}
	res.RejectionReason = reason.String
	return &res, nil
}

func (ar *applicationRepo) GetAll(params *repo.GetAllApplicationsParams) (*repo.GetAllApplicationsResult, error) {
	res := repo.GetAllApplicationsResult{
		Applications: make([]*repo.Application, 0),
	}

	offset := (params.Page - 1) * params.Limit
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)
	filter := " WHERE true "
	if params.VacancyID > 0 {
		filter += fmt.Sprintf(` AND vacancy_id=%d `, params.VacancyID)
	}
	if params.ApplicantID > 0 {
		filter += fmt.Sprintf(` AND applicant_id=%d `, params.ApplicantID)
	}

	query := `
		SELECT 
			id,
			vacancy_id,
			applicant_id,
			status,
			rejection_reason,
			created_at
		FROM applications
		` + filter +
		`ORDER BY created_at DESC
		` + limit

	rows, err := ar.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			a      repo.Application
			reason sql.NullString
		)

		err := rows.Scan(
			&a.ID,
			&a.VacancyID,
			&a.ApplicantID,
			&a.Status,
			&reason,
			&a.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		a.RejectionReason = reason.String
		res.Applications = append(res.Applications, &a)
	}

	queryCount := `SELECT count(1) FROM applications ` + filter
	err = ar.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (ar *applicationRepo) UpdateStatus(a *repo.Application) (*repo.Application, error) {
	query := `
		UPDATE applications SET
			status=$1
		WHERE id=$2 AND vacancy_id=$3
		RETURNING id, vacancy_id, applicant_id, status, rejection_reason, created_at
	`
	var (
		res    repo.Application
		reason sql.NullString
	)
	err := ar.db.QueryRow(
		query,
		a.Status,
		a.ID,
		a.VacancyID,
	).Scan(
		&res.ID,
		&res.VacancyID,
		&res.ApplicantID,
		&res.Status,
		&reason,
		&res.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (ar *applicationRepo) UpdateStatusRejection(a *repo.Application) (*repo.Application, error) {
	query := `
		UPDATE applications SET
			status=$1,
			rejection_reason=$2
		WHERE id=$3 AND vacancy_id=$4
		RETURNING id, vacancy_id, applicant_id, status, rejection_reason, created_at
	`
	var (
		res repo.Application
	)
	err := ar.db.QueryRow(
		query,
		StatusRejected,
		a.RejectionReason,
		a.ID,
		a.VacancyID,
	).Scan(
		&res.ID,
		&res.VacancyID,
		&res.ApplicantID,
		&res.Status,
		&res.RejectionReason,
		&res.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (ar *applicationRepo) Delete(id, vacancyID int64) error {
	query := "DELETE FROM applications WHERE id=$1 AND vacancy_id=$2"

	result, err := ar.db.Exec(query, id, vacancyID)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (ar *applicationRepo) AutoDelete(id int64) error {
	query := "DELETE FROM applications WHERE applicant_id=$1"

	result, err := ar.db.Exec(query, id)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (ar *applicationRepo) GetAllAccordingToStatus(status string) (*repo.GetAllApplicationsResult, error) {
	res := repo.GetAllApplicationsResult{
		Applications: make([]*repo.Application, 0),
	}
	query := fmt.Sprintf(`
		SELECT 
			id,
			vacancy_id,
			applicant_id,
			status,
			rejection_reason,
			created_at
		FROM applications
		WHERE status LIKE '%s'
	`, status)
	rows, err := ar.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			a      repo.Application
			reason sql.NullString
		)

		err := rows.Scan(
			&a.ID,
			&a.VacancyID,
			&a.ApplicantID,
			&a.Status,
			&reason,
			&a.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		a.RejectionReason = reason.String
		res.Applications = append(res.Applications, &a)
	}

	queryCount := `SELECT count(1) FROM applications `
	err = ar.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return &res, nil
}
