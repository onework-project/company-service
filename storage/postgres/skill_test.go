package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/company-service/storage/repo"
)

func createSkill(t *testing.T) *repo.Skill {
	vacancy := createVacancy(t)
	arg := &repo.Skill{
		VacancyID: vacancy.ID,
		Name:      "Backend Developer",
	}
	s, err := dbManager.Skill().Create(arg)

	require.NoError(t, err)
	require.Equal(t, arg.VacancyID, s.VacancyID)
	require.Equal(t, arg.Name, s.Name)

	return s
}

func deleteSkill(t *testing.T, vacancyID int64) {
	err := dbManager.Skill().Delete(vacancyID)
	require.NoError(t, err)
}

func TestCraeteSkill(t *testing.T) {
	s := createSkill(t)
	deleteSkill(t, s.VacancyID)
}

func TestGetAllSkills(t *testing.T) {
	vIDs := []int64{}
	cIDs := []int64{}
	for i := 0; i < 10; i++ {
		v := createVacancy(t)
		l, _ := dbManager.Skill().Create(&repo.Skill{
			VacancyID: v.ID,
			Name:      faker.Word(),
		})
		vIDs = append(vIDs, l.VacancyID)
		cIDs = append(cIDs, v.CompanyID)
	}
	resp, err := dbManager.Skill().GetAll(&repo.GetAllSkillsParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(resp.Skills), 10)

	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteSkill(t, vIDs[i])
		deleteVacancy(t, vIDs[i], cIDs[i])
	}
}
