package postgres_test

import (
	"database/sql"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/company-service/storage/repo"
)

func createCompany(t *testing.T) *repo.Company {
	arg := repo.Company{
		CompanyName:  faker.Name(),
		Description:  faker.Sentence(),
		Email:        faker.Email(),
		PhoneNumber:  faker.Phonenumber(),
		Country:      faker.Name(),
		RegionState:  faker.Name(),
		WorkersCount: "0-1",
		ImageUrl:     faker.URL(),
	}
	company, err := dbManager.Company().Create(&arg)
	require.NoError(t, err)

	require.NotZero(t, company.ID)
	require.Equal(t, arg.CompanyName, company.CompanyName)
	require.Equal(t, arg.Description, company.Description)
	require.Equal(t, arg.Email, company.Email)
	require.Equal(t, arg.PhoneNumber, company.PhoneNumber)
	require.Equal(t, arg.Country, company.Country)
	require.Equal(t, arg.RegionState, company.RegionState)
	require.Equal(t, arg.WorkersCount, company.WorkersCount)
	require.Equal(t, arg.ImageUrl, company.ImageUrl)
	require.NotZero(t, company.CreatedAt)

	return company
}

func deleteCompany(t *testing.T, id int64) {
	err := dbManager.Company().Delete(id)
	require.NoError(t, err)
}

func TestCreateCompany(t *testing.T) {
	company := createCompany(t)
	deleteCompany(t, company.ID)
}

func TestGetCompany(t *testing.T) {
	c := createCompany(t)
	c2, err := dbManager.Company().Get(c.ID)
	require.NoError(t, err)

	require.Equal(t, c.ID, c2.ID)
	require.Equal(t, c.CompanyName, c.CompanyName)
	require.Equal(t, c.Description, c.Description)
	require.Equal(t, c.Email, c.Email)
	require.Equal(t, c.PhoneNumber, c.PhoneNumber)
	require.Equal(t, c.RegionState, c.RegionState)
	require.Equal(t, c.Country, c.Country)
	require.Equal(t, c.WorkersCount, c.WorkersCount)
	require.Equal(t, c.ImageUrl, c.ImageUrl)
	require.NotZero(t, c.CreatedAt)
	deleteCompany(t, c.ID)
}

func TestUpdateCompany(t *testing.T) {
	company := createCompany(t)
	arg := repo.Company{
		ID:           company.ID,
		CompanyName:  faker.Name(),
		Description:  faker.Sentence(),
		Email:        company.Email,
		PhoneNumber:  faker.Phonenumber(),
		Country:      faker.Name(),
		RegionState:  faker.Name(),
		WorkersCount: "100+",
		ImageUrl:     faker.URL(),
	}
	c, err := dbManager.Company().Update(&arg)
	require.NoError(t, err)

	require.Equal(t, arg.ID, c.ID)
	require.NotEqual(t, arg.CompanyName, c.CompanyName)
	require.NotEqual(t, arg.Description, c.Description)
	require.Equal(t, arg.Email, c.Email)
	require.NotEqual(t, arg.PhoneNumber, c.PhoneNumber)
	require.NotEqual(t, arg.RegionState, c.RegionState)
	require.NotEqual(t, arg.Country, c.Country)
	require.NotEqual(t, arg.WorkersCount, c.WorkersCount)
	require.NotEqual(t, arg.ImageUrl, c.ImageUrl)
	require.NotZero(t, c.CreatedAt)
	require.NotZero(t, c.UpdatedAt)

	deleteCompany(t, company.ID)
}

func TestDeleteCompany(t *testing.T) {
	company := createCompany(t)
	err := dbManager.Company().Delete(company.ID)
	require.NoError(t, err)

	_, err = dbManager.Company().Get(company.ID)
	require.Error(t, err)
	require.Equal(t, err, sql.ErrNoRows)

	deleteCompany(t, company.ID)
}

func TestGetAllCompanies(t *testing.T) {
	ids := []int64{}
	for i := 0; i < 10; i++ {
		s1 := createCompany(t)
		ids = append(ids, s1.ID)
	}
	companies, err := dbManager.Company().GetAll(&repo.GetAllCompaniesParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(companies.Companies), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteCompany(t, ids[i])
	}
}
