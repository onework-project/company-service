package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/company-service/storage/repo"
)

type blockedAppRepo struct {
	db *sqlx.DB
}

func NewBlockedApp(db *sqlx.DB) repo.BlockedApplicantI {
	return &blockedAppRepo{
		db: db,
	}
}

func (bar *blockedAppRepo) Create(ba *repo.BlockedApplicant) (*repo.BlockedApplicant, error) {
	query := `
		INSERT INTO blocked_applicants (
			company_id,
			applicant_id
		) VALUES($1, $2)
		RETURNING id, created_at
	`

	err := bar.db.QueryRow(
		query,
		ba.CompanyID,
		ba.ApplicantID,
	).Scan(
		&ba.ID,
		&ba.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return ba, nil
}

func (bar *blockedAppRepo) GetAll(params *repo.GetAllBlockedApplsParams) (*repo.GetAllBlockedApplsRes, error) {
	res := repo.GetAllBlockedApplsRes{
		BlockedAppls: make([]*repo.BlockedApplicant, 0),
	}

	offset := (params.Page - 1) * params.Limit
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)
	filter := ""
	if params.CompanyID > 0 {
		filter = fmt.Sprintf(" WHERE company_id = %d ", params.CompanyID)
	}
	query := `
		SELECT 
			id,
			company_id,
			applicant_id,
			created_at
		FROM blocked_applicants
	` + filter + limit

	rows, err := bar.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var blockedApp repo.BlockedApplicant
		err := rows.Scan(
			&blockedApp.ID,
			&blockedApp.CompanyID,
			&blockedApp.ApplicantID,
			&blockedApp.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		res.BlockedAppls = append(res.BlockedAppls, &blockedApp)
	}

	queryCount := "SELECT count(1) FROM blocked_applicants " + filter

	err = bar.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return &res, nil

}

func (bar *blockedAppRepo) Delete(id, companyID int64) error {
	query := `
		DELETE FROM blocked_applicants WHERE id=$1 and company_id=$2
	`

	result, err := bar.db.Exec(
		query,
		id,
		companyID,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil

}

func (bar *blockedAppRepo) AutoDelete(id int64) error {
	query := `
		DELETE FROM blocked_applicants WHERE applicant_id=$1
	`

	result, err := bar.db.Exec(
		query,
		id,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil

}
