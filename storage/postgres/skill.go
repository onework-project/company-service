package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/company-service/storage/repo"
)

type skillRepo struct {
	db *sqlx.DB
}

func NewSkill(db *sqlx.DB) repo.SkillStorageI {
	return &skillRepo{
		db: db,
	}
}

func (sr *skillRepo) Create(s *repo.Skill) (*repo.Skill, error) {
	query := `
		INSERT INTO skills (
			vacancy_id,
			name
		) VALUES($1, $2)
		RETURNING id
	`

	err := sr.db.QueryRow(
		query,
		s.VacancyID,
		s.Name,
	).Scan(
		&s.ID,
	)
	if err != nil {
		return nil, err
	}

	return s, nil
}

func (sr *skillRepo) Delete(vacancyID int64) error {
	query := "DELETE FROM skills WHERE vacancy_id=$1"

	result, err := sr.db.Exec(query, vacancyID)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (sr *skillRepo) GetAll(params *repo.GetAllSkillsParams) (*repo.GetAllSkillsRes, error) {
	res := repo.GetAllSkillsRes{
		Skills: make([]*repo.Skill, 0),
	}

	offset := (params.Page - 1) * params.Limit
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)
	filter := " WHERE true "
	if params.VacancyID > 0 {
		filter += fmt.Sprintf(` AND vacancy_id=%d `, params.VacancyID)
	}

	query := `
		SELECT 
			id,
			vacancy_id,
			name
		FROM skills
		` + filter + limit

	rows, err := sr.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			s repo.Skill
		)
		err := rows.Scan(
			&s.ID,
			&s.VacancyID,
			&s.Name,
		)
		if err != nil {
			return nil, err
		}

		res.Skills = append(res.Skills, &s)
	}

	queryCount := `SELECT count(1) FROM skills ` + filter
	err = sr.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return &res, nil
}
