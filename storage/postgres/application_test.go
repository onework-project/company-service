package postgres_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/company-service/storage/repo"
)

func createApplication(t *testing.T) *repo.Application {
	vacancy := createVacancy(t)
	arg := &repo.Application{
		VacancyID:   vacancy.ID,
		ApplicantID: 1,
		Status:      "applied",
	}
	ap, err := dbManager.Application().Create(arg)

	require.NoError(t, err)
	require.Equal(t, arg.VacancyID, ap.VacancyID)
	require.Equal(t, arg.ApplicantID, ap.ApplicantID)
	require.Equal(t, arg.Status, ap.Status)

	return ap
}

func deleteApplication(t *testing.T, id, vacancyID int64) {
	err := dbManager.Application().Delete(id, vacancyID)
	require.NoError(t, err)
}

func TestCraeteApplication(t *testing.T) {
	ap := createApplication(t)
	deleteApplication(t, ap.ID, ap.VacancyID)
}

func TestUpdateApplication(t *testing.T) {
	ap := createApplication(t)

	arg := &repo.Application{
		Status:    "screening",
		ID:        ap.ID,
		VacancyID: ap.VacancyID,
	}
	ap2, err := dbManager.Application().UpdateStatus(arg)
	require.NoError(t, err)
	require.Equal(t, arg.ID, ap2.ID)
	require.Equal(t, arg.VacancyID, ap2.VacancyID)
	require.Equal(t, arg.Status, ap2.Status)

	deleteApplication(t, ap2.ID, ap2.VacancyID)
}

func TestGetAllApplications(t *testing.T) {
	ids := []int64{}
	vIDs := []int64{}
	cIDs := []int64{}
	for i := 0; i < 10; i++ {
		v := createVacancy(t)
		ap, _ := dbManager.Application().Create(&repo.Application{
			VacancyID:   v.ID,
			ApplicantID: 11,
		})
		ids = append(ids, ap.ID)
		vIDs = append(vIDs, ap.VacancyID)
		cIDs = append(cIDs, v.CompanyID)
	}
	resp, err := dbManager.Application().GetAll(&repo.GetAllApplicationsParams{
		Limit:     10,
		Page:      1,
	})
	count := 0
	for i := 0; i < len(resp.Applications); i++ {
		count++
	}
	require.GreaterOrEqual(t, count, 10)

	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteApplication(t, ids[i], vIDs[i])
		deleteVacancy(t, vIDs[i], cIDs[i])
	}

}
