package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/company-service/storage/repo"
)

type marketRepo struct {
	db *sqlx.DB
}

func NewMarket(db *sqlx.DB) repo.MarketStorageI {
	return &marketRepo{
		db: db,
	}
}

func (mr *marketRepo) Create(m *repo.Market) (*repo.Market, error) {
	query := `
		INSERT INTO markets (
			vacancy_id,
			market
		) VALUES($1, $2)
		RETURNING id
	`

	err := mr.db.QueryRow(
		query,
		m.VacancyID,
		m.Market,
	).Scan(
		&m.ID,
	)
	if err != nil {
		return nil, err
	}

	return m, nil
}

func (mr *marketRepo) Delete(vacancyID int64) error {
	query := "DELETE FROM markets WHERE vacancy_id=$1"

	result, err := mr.db.Exec(query, vacancyID)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (mr *marketRepo) GetAll(params *repo.GetAllMarketsParams) (*repo.GetAllMarketsRes, error) {
	res := repo.GetAllMarketsRes{
		Markets: make([]*repo.Market, 0),
	}

	offset := (params.Page - 1) * params.Limit
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)
	filter := " WHERE true "
	if params.VacancyID > 0 {
		filter += fmt.Sprintf(` AND vacancy_id=%d `, params.VacancyID)
	}

	query := `
		SELECT 
			id,
			vacancy_id,
			market
		FROM markets
		` + filter + limit

	rows, err := mr.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			m repo.Market
		)
		err := rows.Scan(
			&m.ID,
			&m.VacancyID,
			&m.Market,
		)
		if err != nil {
			return nil, err
		}

		res.Markets = append(res.Markets, &m)
	}

	queryCount := `SELECT count(1) FROM markets ` + filter
	err = mr.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return &res, nil
}
