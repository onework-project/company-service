package postgres_test

import (
	"database/sql"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/company-service/storage/repo"
)

func createVacancy(t *testing.T) *repo.Vacancy {
	company := createCompany(t)
	arg := repo.Vacancy{
		CompanyID:      company.ID,
		Title:          faker.Word(),
		Deadline:       "26/12/2023",
		EmploymentForm: "remote",
		EmploymentType: "full-time",
		Country:        faker.Name(),
		City:           faker.Name(),
		SalaryMin:      100,
		SalaryMax:      1500,
		Currency:       "USD",
		SalaryPeriod:   "month",
		Education:      "certification",
		Experience:     10,
		Description:    faker.Sentence(),
	}
	vacancy, err := dbManager.Vacancy().Create(&arg)
	require.NoError(t, err)

	require.NotZero(t, vacancy.ID)
	require.Equal(t, arg.CompanyID, vacancy.CompanyID)
	require.Equal(t, arg.Title, vacancy.Title)
	require.Equal(t, arg.Deadline, vacancy.Deadline)
	require.Equal(t, arg.EmploymentForm, vacancy.EmploymentForm)
	require.Equal(t, arg.EmploymentType, vacancy.EmploymentType)
	require.Equal(t, arg.Country, vacancy.Country)
	require.Equal(t, arg.City, vacancy.City)
	require.Equal(t, arg.SalaryMin, vacancy.SalaryMin)
	require.Equal(t, arg.SalaryMax, vacancy.SalaryMax)
	require.Equal(t, arg.Currency, vacancy.Currency)
	require.Equal(t, arg.SalaryPeriod, vacancy.SalaryPeriod)
	require.Equal(t, arg.Education, vacancy.Education)
	require.Equal(t, arg.Experience, vacancy.Experience)
	require.Equal(t, arg.Description, vacancy.Description)
	require.NotZero(t, vacancy.CreatedAt)

	return vacancy
}

func deleteVacancy(t *testing.T, id, companyID int64) {
	err := dbManager.Vacancy().Delete(id, companyID)
	require.NoError(t, err)
}

func TestCreateVacany(t *testing.T) {
	vacancy := createVacancy(t)
	deleteVacancy(t, vacancy.ID, vacancy.CompanyID)
}

func TestGetVacancy(t *testing.T) {
	arg := createVacancy(t)
	vacancy, err := dbManager.Vacancy().Get(arg.ID)
	require.NoError(t, err)

	require.Equal(t, arg.ID, vacancy.ID)
	require.Equal(t, arg.CompanyID, vacancy.CompanyID)
	require.Equal(t, arg.Title, vacancy.Title)
	require.Equal(t, arg.Deadline, vacancy.Deadline)
	require.Equal(t, arg.EmploymentForm, vacancy.EmploymentForm)
	require.Equal(t, arg.EmploymentType, vacancy.EmploymentType)
	require.Equal(t, arg.Country, vacancy.Country)
	require.Equal(t, arg.City, vacancy.City)
	require.Equal(t, arg.SalaryMin, vacancy.SalaryMin)
	require.Equal(t, arg.SalaryMax, vacancy.SalaryMax)
	require.Equal(t, arg.Currency, vacancy.Currency)
	require.Equal(t, arg.SalaryPeriod, vacancy.SalaryPeriod)
	require.Equal(t, arg.Education, vacancy.Education)
	require.Equal(t, arg.Experience, vacancy.Experience)
	require.Equal(t, arg.Description, vacancy.Description)
	require.NotZero(t, vacancy.CreatedAt)
	deleteVacancy(t, arg.ID, arg.CompanyID)
}

func TestUpdateVacancy(t *testing.T) {
	v1 := createVacancy(t)
	arg := repo.Vacancy{
		ID:             v1.ID,
		CompanyID:      v1.CompanyID,
		Title:          faker.Word(),
		Deadline:       "30/10/2023",
		EmploymentForm: "hybrid",
		EmploymentType: "freelance",
		Country:        faker.Name(),
		City:           faker.Name(),
		SalaryMin:      200,
		SalaryMax:      2000,
		Currency:       "EUR",
		SalaryPeriod:   "hour",
		Experience:     10,
		Description:    faker.Sentence(),
	}
	v2, err := dbManager.Vacancy().Update(&arg)
	require.NoError(t, err)

	require.Equal(t, v1.ID, v2.ID)
	require.Equal(t, arg.CompanyID, v2.CompanyID)
	require.Equal(t, arg.Title, v2.Title)
	require.Equal(t, arg.Deadline, v2.Deadline)
	require.Equal(t, arg.EmploymentForm, v2.EmploymentForm)
	require.Equal(t, arg.EmploymentType, v2.EmploymentType)
	require.Equal(t, arg.Country, v2.Country)
	require.Equal(t, arg.City, v2.City)
	require.Equal(t, arg.SalaryMin, v2.SalaryMin)
	require.Equal(t, arg.SalaryMax, v2.SalaryMax)
	require.Equal(t, arg.Currency, v2.Currency)
	require.Equal(t, arg.SalaryPeriod, v2.SalaryPeriod)
	require.Equal(t, arg.Experience, v2.Experience)
	require.Equal(t, arg.Description, v2.Description)
	require.NotZero(t, v2.UpdatedAt)

	deleteVacancy(t, v1.ID, v1.CompanyID)
}

func TestDeleteVacancy(t *testing.T) {
	vacancy := createVacancy(t)
	err := dbManager.Vacancy().Delete(vacancy.ID, vacancy.CompanyID)
	require.NoError(t, err)

	_, err = dbManager.Vacancy().Get(vacancy.ID)
	require.Error(t, err)
	require.Equal(t, err, sql.ErrNoRows)
}

func TestDisActivateOrActivate(t *testing.T) {
	v1 := createVacancy(t)
	v2, err := dbManager.Vacancy().ActivateOrDisactivate(v1.ID, v1.CompanyID, false)
	require.NoError(t, err)

	require.Equal(t, v1.ID, v2.ID)
	require.Equal(t, v1.CompanyID, v2.CompanyID)
	require.Equal(t, v1.Title, v2.Title)
	require.Equal(t, v1.Deadline, v2.Deadline)
	require.Equal(t, v1.EmploymentForm, v2.EmploymentForm)
	require.Equal(t, v1.EmploymentType, v2.EmploymentType)
	require.Equal(t, v1.Country, v2.Country)
	require.Equal(t, v1.City, v2.City)
	require.Equal(t, v1.SalaryMin, v2.SalaryMin)
	require.Equal(t, v1.SalaryMax, v2.SalaryMax)
	require.Equal(t, v1.Currency, v2.Currency)
	require.Equal(t, v1.SalaryPeriod, v2.SalaryPeriod)
	require.Equal(t, v1.Education, v2.Education)
	require.Equal(t, v1.Experience, v2.Experience)
	require.Equal(t, v1.Description, v2.Description)
	require.NotEqual(t, v1.IsActive, v2.IsActive)

	deleteVacancy(t, v1.ID, v2.CompanyID)
}

func TestGetAllVacancies(t *testing.T) {
	IDs := []int64{}
	CIDs := []int64{}
	for i := 0; i < 10; i++ {
		s1 := createVacancy(t)
		IDs = append(IDs, s1.ID)
		CIDs = append(CIDs, s1.CompanyID)
	}
	vacancies, err := dbManager.Vacancy().GetAllActive(&repo.GetAllVacanciesParams{
		Limit: 10,
		Page:  1,
	})
	require.NoError(t, err)
	require.GreaterOrEqual(t, len(vacancies.Vacancies), 10)
	for i := 0; i < 10; i++ {
		deleteVacancy(t, IDs[i], CIDs[i])
	}
}

func TestGetAllDisactive(t *testing.T) {
	IDs := []int64{}
	CIDs := []int64{}
	for i := 0; i < 10; i++ {
		s1 := createVacancy(t)
		IDs = append(IDs, s1.ID)
		CIDs = append(CIDs, s1.CompanyID)
		v, err := dbManager.Vacancy().ActivateOrDisactivate(IDs[i], CIDs[i], false)
		require.NoError(t, err)
		require.NotEmpty(t, v)
	}
	vacancies, err := dbManager.Vacancy().GetAllDisactive(&repo.GetAllDisactiveParams{
		Limit: 10,
		Page:  1,
	})
	require.NoError(t, err)
	require.GreaterOrEqual(t, len(vacancies.Vacancies), 10)
	for i := 0; i < 10; i++ {
		deleteVacancy(t, IDs[i], CIDs[i])
	}
}

func TestAddView(t *testing.T) {
	v := createVacancy(t)
	err := dbManager.Vacancy().AddView(v.ID)
	require.NoError(t, err)

	deleteVacancy(t, v.ID, v.CompanyID)
}
