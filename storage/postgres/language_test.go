package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/company-service/storage/repo"
)

func createLanguage(t *testing.T) *repo.Language {
	vacancy := createVacancy(t)
	arg := &repo.Language{
		VacancyID: vacancy.ID,
		Language:  faker.Language,
		Level:     "Native",
	}
	l, err := dbManager.Language().Create(arg)

	require.NoError(t, err)
	require.Equal(t, arg.VacancyID, l.VacancyID)
	require.Equal(t, arg.Language, l.Language)
	require.Equal(t, arg.Level, l.Level)

	return l
}

func deleteLanguage(t *testing.T, VacancyID int64) {
	err := dbManager.Language().Delete(VacancyID)
	require.NoError(t, err)
}

func TestCraeteLanguage(t *testing.T) {
	l := createLanguage(t)
	deleteLanguage(t, l.VacancyID)
}

func TestGetAllLanguages(t *testing.T) {
	vIDs := []int64{}
	cIDs := []int64{}
	for i := 0; i < 10; i++ {
		v := createVacancy(t)
		l, _ := dbManager.Language().Create(&repo.Language{
			VacancyID: v.ID,
			Language:  faker.Language,
			Level:     "B2",
		})
		vIDs = append(vIDs, l.VacancyID)
		cIDs = append(cIDs, v.CompanyID)
	}
	resp, err := dbManager.Language().GetAll(&repo.GetAllLanguagesParams{
		Limit: 10,
		Page:  1,
	})
	count := 0
	for i := 0; i < len(resp.Languages); i++ {
		count++
	}
	require.GreaterOrEqual(t, count, 10)

	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteLanguage(t, vIDs[i])
		deleteVacancy(t, vIDs[i], cIDs[i])
	}
}
