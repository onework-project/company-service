package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/company-service/pkg/utils"
	"gitlab.com/onework-project/company-service/storage/repo"
)

type companyRepo struct {
	db *sqlx.DB
}

func NewCompany(db *sqlx.DB) repo.CompanyStorageI {
	return &companyRepo{
		db: db,
	}
}

func (cr *companyRepo) Create(c *repo.Company) (*repo.Company, error) {
	var (
		updatedAt sql.NullTime
	)
	query := `
		INSERT INTO companies (
			company_name,
			description,
			email,
			phone_number,
			country,
			region_state,
			workers_count,
			image_url,
			slug
		) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)
		RETURNING id, created_at, updated_at
	`

	err := cr.db.QueryRow(
		query,
		c.CompanyName,
		c.Description,
		c.Email,
		utils.NullString(c.PhoneNumber),
		c.Country,
		c.RegionState,
		c.WorkersCount,
		c.ImageUrl,
		c.Slug,
	).Scan(
		&c.ID,
		&c.CreatedAt,
		&updatedAt,
	)
	if err != nil {
		return nil, err
	}
	c.UpdatedAt = updatedAt.Time

	return c, nil
}

func (cr *companyRepo) Get(id int64) (*repo.Company, error) {
	var (
		res         repo.Company
		updatedAt   sql.NullTime
		phoneNumber sql.NullString
	)
	query := `
		SELECT 
			id,
			company_name,
			description,
			email,
			phone_number,
			country,
			region_state,
			workers_count,
			image_url,
			posted,
			created_at,
			updated_at,
			slug
		FROM companies	
		WHERE id=$1
	`

	err := cr.db.QueryRow(query, id).Scan(
		&res.ID,
		&res.CompanyName,
		&res.Description,
		&res.Email,
		&phoneNumber,
		&res.Country,
		&res.RegionState,
		&res.WorkersCount,
		&res.ImageUrl,
		&res.Posts,
		&res.CreatedAt,
		&updatedAt,
		&res.Slug,
	)
	if err != nil {
		return nil, err
	}
	res.UpdatedAt = updatedAt.Time
	res.PhoneNumber = phoneNumber.String
	return &res, nil
}

func (cr *companyRepo) Update(c *repo.Company) (*repo.Company, error) {
	query := `
		UPDATE companies SET
			company_name=$1,
			description=$2,
			phone_number=$3,
			country=$4,
			region_state=$5,
			workers_count=$6,
			image_url=$7,
			email = $8,
			updated_at = CURRENT_TIMESTAMP,
			slug = $9
		WHERE id=$10
		RETURNING created_at, updated_at
	`
	err := cr.db.QueryRow(
		query,
		c.CompanyName,
		c.Description,
		utils.NullString(c.PhoneNumber),
		c.Country,
		c.RegionState,
		c.WorkersCount,
		c.ImageUrl,
		c.Email,
		c.Slug,
		c.ID,
	).Scan(
		&c.CreatedAt,
		&c.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (cr *companyRepo) Delete(id int64) error {
	query := "DELETE FROM companies WHERE id=$1"

	result, err := cr.db.Exec(query, id)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (cr *companyRepo) GetAll(params *repo.GetAllCompaniesParams) (*repo.GetAllCompaniesResult, error) {
	res := repo.GetAllCompaniesResult{
		Companies: make([]*repo.Company, 0),
	}

	offset := (params.Page - 1) * params.Limit
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)
	filter := " WHERE true "
	if params.CompanyName != "" {
		str := "%" + params.CompanyName + "%"
		filter += fmt.Sprintf(`
			AND company_name ILIKE '%s' 
		`, str)
	}

	if params.Location != "" {
		str := "%" + params.Location + "%"
		filter += fmt.Sprintf(`
		AND country ILIKE '%s' OR  region_state ILIKE '%s'
		`, str, str)
	}

	query := `
		SELECT 
			id,
			company_name,
			description,
			email,
			phone_number,
			country,
			region_state,
			workers_count,
			image_url,
			created_at,
			updated_at,
			slug
		FROM companies
		` + filter +
		`ORDER BY company_name ASC
		` + limit

	rows, err := cr.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			c           repo.Company
			updatedAt   sql.NullTime
			phoneNumber sql.NullString
		)
		err := rows.Scan(
			&c.ID,
			&c.CompanyName,
			&c.Description,
			&c.Email,
			&phoneNumber,
			&c.Country,
			&c.RegionState,
			&c.WorkersCount,
			&c.ImageUrl,
			&c.CreatedAt,
			&updatedAt,
			&c.Slug,
		)
		if err != nil {
			return nil, err
		}
		c.UpdatedAt = updatedAt.Time
		c.PhoneNumber = phoneNumber.String

		res.Companies = append(res.Companies, &c)
	}

	queryCount := `SELECT count(1) FROM companies ` + filter
	err = cr.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (ar *companyRepo) GetSlug(slug string) (int64, error) {
	var id int64
	query := fmt.Sprintf(" SELECT id FROM companies WHERE slug = '%s' ", slug)
	err := ar.db.QueryRow(query).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}
