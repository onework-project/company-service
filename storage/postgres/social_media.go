package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/company-service/storage/repo"
)

type smRepo struct {
	db *sqlx.DB
}

func NewSM(db *sqlx.DB) repo.SMStorageI {
	return &smRepo{
		db: db,
	}
}

func (s *smRepo) Create(sm *repo.SocialMedia) (*repo.SocialMedia, error) {
	query := `
		INSERT INTO social_medias (
			company_id,
			name,
			url
		) VALUES($1, $2, $3)
		RETURNING id, company_id, name 
	`

	err := s.db.QueryRow(
		query,
		sm.CompanyID,
		sm.Name,
		sm.Url,
	).Scan(
		&sm.ID,
		&sm.CompanyID,
		&sm.Name,
	)
	if err != nil {
		return nil, err
	}

	return sm, nil
}

func (s *smRepo) Delete(CompanyID int64) error {
	query := "DELETE FROM social_medias WHERE company_id = $1"

	result, err := s.db.Exec(query, CompanyID)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (s *smRepo) GetAllSMs(params *repo.GetAllSMsParams) (*repo.GetAllSMsRes, error) {
	res := repo.GetAllSMsRes{
		SMs: make([]*repo.SocialMedia, 0),
	}

	offset := (params.Page - 1) * params.Limit
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)
	filter := ""
	if params.CompanyID > 0 {
		filter = fmt.Sprintf(" WHERE company_id=%d ", params.CompanyID)
	}

	query := `
		SELECT 
			id,
			company_id,
			name,
			url
		FROM social_medias
		` + filter + limit

	rows, err := s.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			sm   repo.SocialMedia
			Name sql.NullString
		)
		err := rows.Scan(
			&sm.ID,
			&sm.CompanyID,
			&Name,
			&sm.Url,
		)
		if err != nil {
			return nil, err
		}
		sm.Name = Name.String

		res.SMs = append(res.SMs, &sm)
	}

	queryCount := `SELECT count(1) FROM social_medias ` + filter
	err = s.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return &res, nil
}
