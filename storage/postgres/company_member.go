package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/company-service/storage/repo"
)

const (
	CompanyOwner = "owner"
	CompanyAdmin = "admin"
)

type companyMemberRepo struct {
	db *sqlx.DB
}

func NewCompanyMember(db *sqlx.DB) repo.CompanyMemberStorageI {
	return &companyMemberRepo{
		db: db,
	}
}

func (cr *companyMemberRepo) AddMember(c *repo.Member) (*repo.Member, error) {
	query := `
		INSERT INTO company_members (
			company_id,
			member_id,
			user_type
		) VALUES($1, $2, $3)
		RETURNING id, created_at
	`

	err := cr.db.QueryRow(
		query,
		c.CompanyID,
		c.MemberID,
		c.UserType,
	).Scan(
		&c.ID,
		&c.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (cr *companyMemberRepo) GetMember(id int64) (*repo.Member, error) {
	var (
		res repo.Member
	)
	query := `
		SELECT 
			id,
			company_id,
			member_id,
			user_type,
			created_at
		FROM company_members	
		WHERE member_id=$1
	`

	err := cr.db.QueryRow(query, id).Scan(
		&res.ID,
		&res.CompanyID,
		&res.MemberID,
		&res.UserType,
		&res.CreatedAt,
	)
	if err != nil {
		return nil, err
	}
	return &res, nil
}

func (cr *companyMemberRepo) ExchangeMemberType(c *repo.ExchangeType) error {
	tr, err := cr.db.Begin()
	if err != nil {
		return err
	}
	defer tr.Rollback()
	queryOwner := `
		UPDATE company_members SET
			user_type = $1
		WHERE member_id = $2
	`
	queryAdmin := `
		UPDATE company_members SET 
			user_type = $1
		WHERE member_id = $2
	`
	res, err := tr.Exec(
		queryOwner,
		CompanyAdmin,
		c.OwnerID,
	)
	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}
	res, err = tr.Exec(
		queryAdmin,
		CompanyOwner,
		c.AdminID,
	)
	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	tr.Commit()
	return nil
}

func (cr *companyMemberRepo) DeleteMember(id int64) error {
	tr, err := cr.db.Begin()
	if err != nil {
		return err
	}
	defer tr.Rollback()

	query := "DELETE FROM company_members WHERE member_id = $1"
	result, err := tr.Exec(query, id)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	tr.Commit()
	return nil
}

func (cr *companyMemberRepo) GetAllMembers(params *repo.GetAllMembersParams) (*repo.GetAllMembers, error) {
	res := repo.GetAllMembers{
		Members: make([]*repo.Member, 0),
	}

	offset := (params.Page - 1) * params.Limit
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)
	filter := ""
	if params.CompanyID > 0 {
		filter = fmt.Sprintf(" WHERE company_id = %d", params.CompanyID)
	}

	query := `
		SELECT 
			id,
			company_id,
			member_id,
			user_type,
			created_at
		FROM company_members
		` + filter +
		` ORDER BY created_at ASC ` + limit

	rows, err := cr.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			c repo.Member
		)
		err := rows.Scan(
			&c.ID,
			&c.CompanyID,
			&c.MemberID,
			&c.UserType,
			&c.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		res.Members = append(res.Members, &c)
	}

	queryCount := `SELECT count(1) FROM company_members ` + filter
	err = cr.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return &res, nil
}
