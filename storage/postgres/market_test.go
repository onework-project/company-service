package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/company-service/storage/repo"
)

func createMarket(t *testing.T) *repo.Market {
	vacancy := createVacancy(t)
	arg := &repo.Market{
		VacancyID: vacancy.ID,
		Market:    "IT",
	}
	m, err := dbManager.Market().Create(arg)

	require.NoError(t, err)
	require.Equal(t, arg.VacancyID, m.VacancyID)
	require.Equal(t, arg.Market, m.Market)

	return m
}

func deleteMarket(t *testing.T, vacancyID int64) {
	err := dbManager.Market().Delete(vacancyID)
	require.NoError(t, err)
}

func TestCraeteMarket(t *testing.T) {
	m := createMarket(t)
	deleteMarket(t, m.VacancyID)
}

func TestGetAllMarkets(t *testing.T) {
	vIDs := []int64{}
	cIDs := []int64{}
	for i := 0; i < 10; i++ {
		v := createVacancy(t)
		l, _ := dbManager.Market().Create(&repo.Market{
			VacancyID: v.ID,
			Market:    faker.Word(),
		})
		vIDs = append(vIDs, l.VacancyID)
		cIDs = append(cIDs, v.CompanyID)
	}
	resp, err := dbManager.Market().GetAll(&repo.GetAllMarketsParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(resp.Markets), 10)

	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteMarket(t, vIDs[i])
		deleteVacancy(t, vIDs[i], cIDs[i])
	}
}
