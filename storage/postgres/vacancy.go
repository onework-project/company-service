package postgres

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/company-service/pkg/utils"
	"gitlab.com/onework-project/company-service/storage/repo"
)

const (
	pastDay   = "past_day"
	pastWeek  = "past_week"
	pastMonth = "past_month"
)

type vacancyRepo struct {
	db *sqlx.DB
}

func NewVacancy(db *sqlx.DB) repo.VacancyStorageI {
	return &vacancyRepo{
		db: db,
	}
}

func (vr *vacancyRepo) Create(v *repo.Vacancy) (*repo.Vacancy, error) {
	query := `
		INSERT INTO vacancies (
			company_id,
			title,
			deadline,
			employment_form,
			employment_type,
			country,
			city,
			salary_min,   
			salary_max,   
			currency,    
			salary_period,
			education,   
			experience,  
			description,
			slug
		) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)
		RETURNING id, is_active, views_count, created_at, updated_at
	`
	var (
		updatedAt sql.NullTime
	)
	err := vr.db.QueryRow(
		query,
		v.CompanyID,
		v.Title,
		v.Deadline,
		v.EmploymentForm,
		v.EmploymentType,
		v.Country,
		v.City,
		v.SalaryMin,
		v.SalaryMax,
		v.Currency,
		v.SalaryPeriod,
		utils.NullString(v.Education),
		v.Experience,
		v.Description,
		v.Slug,
	).Scan(
		&v.ID,
		&v.IsActive,
		&v.ViewCount,
		&v.CreatedAt,
		&updatedAt,
	)
	if err != nil {
		return nil, err
	}
	v.UpdatedAt = updatedAt.Time

	queryIncrement := `
		UPDATE companies SET posted = posted + 1 WHERE id = $1
	`
	res, err := vr.db.Exec(queryIncrement, v.CompanyID)
	if err != nil {
		return nil, err
	}

	if count, _ := res.RowsAffected(); count == 0 {
		return nil, sql.ErrNoRows
	}
	return v, nil
}

func (vr *vacancyRepo) Get(id int64) (*repo.Vacancy, error) {
	var (
		res       repo.Vacancy
		updatedAt sql.NullTime
		education sql.NullString
	)
	query := `
		SELECT 
			id,
			company_id,
			title,
			deadline,
			employment_form,
			employment_type,
			country,
			city,
			salary_min,   
			salary_max,   
			currency,    
			salary_period,
			education,   
			experience,  
			description, 
			is_active,
			views_count,
			created_at,
			updated_at,
			slug
		FROM vacancies	
		WHERE id=$1
	`

	err := vr.db.QueryRow(query, id).Scan(
		&res.ID,
		&res.CompanyID,
		&res.Title,
		&res.Deadline,
		&res.EmploymentForm,
		&res.EmploymentType,
		&res.Country,
		&res.City,
		&res.SalaryMin,
		&res.SalaryMax,
		&res.Currency,
		&res.SalaryPeriod,
		&education,
		&res.Experience,
		&res.Description,
		&res.IsActive,
		&res.ViewCount,
		&res.CreatedAt,
		&updatedAt,
		&res.Slug,
	)
	if err != nil {
		return nil, err
	}
	res.UpdatedAt = updatedAt.Time
	res.Education = education.String
	return &res, nil
}

func (vr *vacancyRepo) JustGet(id int64) (*repo.Vacancy, error) {
	var (
		res       repo.Vacancy
		updatedAt sql.NullTime
		education sql.NullString
	)
	query := `
		SELECT 
			id,
			company_id,
			title,
			deadline,
			employment_form,
			employment_type,
			country,
			city,
			salary_min,   
			salary_max,   
			currency,    
			salary_period,
			education,   
			experience,  
			description, 
			is_active,
			views_count,
			created_at,
			updated_at,
			slug
		FROM vacancies	
		WHERE id=$1
	`

	err := vr.db.QueryRow(query, id).Scan(
		&res.ID,
		&res.CompanyID,
		&res.Title,
		&res.Deadline,
		&res.EmploymentForm,
		&res.EmploymentType,
		&res.Country,
		&res.City,
		&res.SalaryMin,
		&res.SalaryMax,
		&res.Currency,
		&res.SalaryPeriod,
		&education,
		&res.Experience,
		&res.Description,
		&res.IsActive,
		&res.ViewCount,
		&res.CreatedAt,
		&updatedAt,
		&res.Slug,
	)
	if err != nil {
		return nil, err
	}
	res.UpdatedAt = updatedAt.Time
	res.Education = education.String
	return &res, nil
}

func (vr *vacancyRepo) Update(c *repo.Vacancy) (*repo.Vacancy, error) {
	query := `
		UPDATE vacancies SET
			title = $1,
			deadline = $2,
			employment_form = $3,
			employment_type = $4,
			country = $5,
			city = $6,
			salary_min = $7,   
			salary_max = $8,   
			currency = $9,    
			salary_period = $10,
			education = $11,   
			experience = $12,  
			description = $13,  
			updated_at = CURRENT_TIMESTAMP,
			slug = $14
		WHERE id=$15
		RETURNING is_active, views_count, created_at, updated_at
	`
	err := vr.db.QueryRow(
		query,
		c.Title,
		c.Deadline,
		c.EmploymentForm,
		c.EmploymentType,
		c.Country,
		c.City,
		c.SalaryMin,
		c.SalaryMax,
		c.Currency,
		c.SalaryPeriod,
		utils.NullString(c.Education),
		c.Experience,
		c.Description,
		c.Slug,
		c.ID,
	).Scan(
		&c.IsActive,
		&c.ViewCount,
		&c.CreatedAt,
		&c.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (vr *vacancyRepo) Delete(id, companyID int64) error {
	query := "DELETE FROM vacancies WHERE id=$1 AND company_id = $2"

	result, err := vr.db.Exec(query, id, companyID)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (vr *vacancyRepo) ActivateOrDisactivate(id, CompanyID int64, smth bool) (*repo.Vacancy, error) {
	query := `
		UPDATE vacancies SET is_active = $1 WHERE id = $2 AND company_id = $3
		RETURNING 
			id, company_id,title,deadline,
			employment_form,employment_type,country,city,
			salary_min,salary_max, currency,salary_period, education,   
			experience,description, is_active,views_count,
			created_at,updated_at,slug
	`
	var (
		res       repo.Vacancy
		updatedAt sql.NullTime
		education sql.NullString
	)
	err := vr.db.QueryRow(
		query,
		smth,
		id,
		CompanyID,
	).Scan(
		&res.ID,
		&res.CompanyID,
		&res.Title,
		&res.Deadline,
		&res.EmploymentForm,
		&res.EmploymentType,
		&res.Country,
		&res.City,
		&res.SalaryMin,
		&res.SalaryMax,
		&res.Currency,
		&res.SalaryPeriod,
		&education,
		&res.Experience,
		&res.Description,
		&res.IsActive,
		&res.ViewCount,
		&res.CreatedAt,
		&updatedAt,
		&res.Slug,
	)
	if err != nil {
		return nil, err
	}
	res.UpdatedAt = updatedAt.Time
	res.Education = education.String
	return &res, nil
}

func (vr *vacancyRepo) GetAllActive(params *repo.GetAllVacanciesParams) (*repo.GetAllVacanciesRes, error) {
	res := repo.GetAllVacanciesRes{
		Vacancies: make([]*repo.Vacancy, 0),
	}

	offset := (params.Page - 1) * params.Limit
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)
	filter := getAllVacanciesParams(params)

	query := `
		SELECT 
			v.id,
			v.company_id,
			v.title,
			v.deadline,
			v.employment_form,
			v.employment_type,
			v.country,
			v.city,
			v.salary_min,   
			v.salary_max,   
			v.currency,    
			v.salary_period,
			v.education,   
			v.experience,  
			v.description, 
			v.is_active,
			v.views_count,
			v.created_at,
			v.updated_at,
			v.slug
		FROM vacancies v
		` + filter +
		`ORDER BY v.created_at DESC
		` + limit
	rows, err := vr.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	log.Println(query)
	for rows.Next() {
		var (
			v         repo.Vacancy
			updatedAt sql.NullTime
			education sql.NullString
		)

		err := rows.Scan(
			&v.ID,
			&v.CompanyID,
			&v.Title,
			&v.Deadline,
			&v.EmploymentForm,
			&v.EmploymentType,
			&v.Country,
			&v.City,
			&v.SalaryMin,
			&v.SalaryMax,
			&v.Currency,
			&v.SalaryPeriod,
			&education,
			&v.Experience,
			&v.Description,
			&v.IsActive,
			&v.ViewCount,
			&v.CreatedAt,
			&updatedAt,
			&v.Slug,
		)
		if err != nil {
			return nil, err
		}
		v.Education = education.String
		v.UpdatedAt = updatedAt.Time
		res.Vacancies = append(res.Vacancies, &v)
	}

	queryCount := ` SELECT count(1) FROM vacancies v ` + filter
	err = vr.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (vr *vacancyRepo) GetAllDisactive(params *repo.GetAllDisactiveParams) (*repo.GetAllVacanciesRes, error) {
	res := repo.GetAllVacanciesRes{
		Vacancies: make([]*repo.Vacancy, 0),
	}
	offset := (params.Page - 1) * params.Limit
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)
	filter := " WHERE is_active = false "
	if params.CompanyID > 0 {
		filter += fmt.Sprintf(` AND company_id=%d `, params.CompanyID)
	}

	query := `
		SELECT 
			id,
			company_id,
			title,
			deadline,
			employment_form,
			employment_type,
			country,
			city,
			salary_min,   
			salary_max,   
			currency,    
			salary_period,
			education,   
			experience,  
			description, 
			is_active,
			views_count,
			created_at,
			updated_at,
			slug
		FROM vacancies
		` + filter +
		` ORDER BY created_at DESC
		` + limit
	rows, err := vr.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			v         repo.Vacancy
			updatedAt sql.NullTime
			education sql.NullString
		)

		err := rows.Scan(
			&v.ID,
			&v.CompanyID,
			&v.Title,
			&v.Deadline,
			&v.EmploymentForm,
			&v.EmploymentType,
			&v.Country,
			&v.City,
			&v.SalaryMin,
			&v.SalaryMax,
			&v.Currency,
			&v.SalaryPeriod,
			&education,
			&v.Experience,
			&v.Description,
			&v.IsActive,
			&v.ViewCount,
			&v.CreatedAt,
			&updatedAt,
			&v.Slug,
		)
		if err != nil {
			return nil, err
		}
		v.Education = education.String
		v.UpdatedAt = updatedAt.Time
		res.Vacancies = append(res.Vacancies, &v)
	}

	queryCount := `SELECT count(1) FROM vacancies ` + filter
	err = vr.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}
	return &res, nil
}

func (vr *vacancyRepo) AddView(id int64) error {
	_, err := vr.db.Exec(
		`UPDATE vacancies SET views_count=views_count+1
			WHERE id=$1`, id,
	)
	if err != nil {
		return err
	}

	return nil
}

func (ar *vacancyRepo) GetSlug(slug string) (int64, error) {
	var id int64
	query := fmt.Sprintf(" SELECT id FROM vacancies WHERE slug = '%s' ", slug)
	err := ar.db.QueryRow(query).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func getAllVacanciesParams(params *repo.GetAllVacanciesParams) string {
	filter := ` WHERE v.is_active = true `
	if params.CompanyID > 0 {
		filter += fmt.Sprintf(`
			AND v.company_id = %d 
		`, params.CompanyID)
	}
	if params.Title != "" {
		filter += fmt.Sprintf(`
			AND v.title ILIKE '%s'
			OR v.id IN 
			(SELECT s.vacancy_id FROM skills s WHERE s.name ILIKE '%s' )
		`, "%"+params.Title+"%", "%"+params.Title+"%")
	}
	if params.EmploymentForm != "" {
		filter += fmt.Sprintf(`
			AND  v.employment_form ILIKE '%s'
		`, "%"+params.EmploymentForm+"%")
	}
	if params.EmploymentType != "" {
		filter += fmt.Sprintf(`
			AND  v.employment_type ILIKE '%s'
		`, "%"+params.EmploymentType+"%")
	}
	if params.Market != "" {
		str := "%" + params.Market + "%"
		filter += fmt.Sprintf(`
			AND v.id IN 
			(SELECT m.vacancy_id FROM markets m WHERE m.market ILIKE '%s' )
			`, str)
	}
	if params.Location != "" {
		filter += fmt.Sprintf(`
			AND v.country ILIKE '%s' OR v.city ILIKE '%s'
		`, "%"+params.Location+"%", "%"+params.Location+"%")
	}

	if params.YearsOfExperience >= 0 {
		filter += fmt.Sprintf(`
			AND v.experience >= %d
		`, params.YearsOfExperience)
	}
	if params.TimeRange != "" {
		switch params.TimeRange {
		case pastDay:
			filter += " AND v.created_at BETWEEN NOW() - INTERVAL '24 HOURS' AND NOW() "
		case pastWeek:
			filter += " AND v.created_at BETWEEN NOW() - INTERVAL '168 HOURS' AND NOW() "
		case pastMonth:
			filter += " AND v.created_at BETWEEN NOW() - INTERVAL '730 HOURS' AND NOW() "
		}
	}

	return filter
}
