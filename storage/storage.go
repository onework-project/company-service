package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/company-service/storage/postgres"
	"gitlab.com/onework-project/company-service/storage/repo"
)

type StorageI interface {
	Company() repo.CompanyStorageI
	CompanyMember() repo.CompanyMemberStorageI
	Vacancy() repo.VacancyStorageI
	SocialMedia() repo.SMStorageI
	Language() repo.LanguageStorageI
	Application() repo.ApplicationStorageI
	Market() repo.MarketStorageI
	Skill() repo.SkillStorageI
	BlockedApplicant() repo.BlockedApplicantI
}

type storagePg struct {
	companyRepo       repo.CompanyStorageI
	companyMemberRepo repo.CompanyMemberStorageI
	vacancyRepo       repo.VacancyStorageI
	smRepo            repo.SMStorageI
	languageRepo      repo.LanguageStorageI
	applicationRepo   repo.ApplicationStorageI
	marketRepo        repo.MarketStorageI
	skillRepo         repo.SkillStorageI
	blockedAppRepo    repo.BlockedApplicantI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		companyRepo:       postgres.NewCompany(db),
		vacancyRepo:       postgres.NewVacancy(db),
		smRepo:            postgres.NewSM(db),
		languageRepo:      postgres.NewLanguage(db),
		applicationRepo:   postgres.NewApplicant(db),
		marketRepo:        postgres.NewMarket(db),
		skillRepo:         postgres.NewSkill(db),
		blockedAppRepo:    postgres.NewBlockedApp(db),
		companyMemberRepo: postgres.NewCompanyMember(db),
	}
}

func (s *storagePg) Company() repo.CompanyStorageI {
	return s.companyRepo
}

func (s *storagePg) CompanyMember() repo.CompanyMemberStorageI {
	return s.companyMemberRepo
}

func (s *storagePg) Vacancy() repo.VacancyStorageI {
	return s.vacancyRepo
}

func (s *storagePg) SocialMedia() repo.SMStorageI {
	return s.smRepo
}

func (s *storagePg) Language() repo.LanguageStorageI {
	return s.languageRepo
}

func (s *storagePg) Application() repo.ApplicationStorageI {
	return s.applicationRepo
}

func (s *storagePg) Market() repo.MarketStorageI {
	return s.marketRepo
}

func (s *storagePg) Skill() repo.SkillStorageI {
	return s.skillRepo
}

func (s *storagePg) BlockedApplicant() repo.BlockedApplicantI {
	return s.blockedAppRepo
}
