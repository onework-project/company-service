package repo

type SkillStorageI interface {
	Create(s *Skill) (*Skill, error)
	Delete(vacancyID int64) error
	GetAll(params *GetAllSkillsParams) (*GetAllSkillsRes, error)
}

type Skill struct {
	ID        int64
	VacancyID int64
	Name      string
}

type GetAllSkillsParams struct {
	Limit     int32
	Page      int32
	VacancyID int64
}

type GetAllSkillsRes struct {
	Skills []*Skill
	Count  int32
}
