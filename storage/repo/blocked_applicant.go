package repo

import "time"

type BlockedApplicantI interface {
	Create(ba *BlockedApplicant) (*BlockedApplicant, error)
	GetAll(params *GetAllBlockedApplsParams) (*GetAllBlockedApplsRes, error)
	Delete(id, companyID int64) error
	AutoDelete(id int64) error
}

type BlockedApplicant struct {
	ID          int64
	CompanyID   int64
	ApplicantID int64
	CreatedAt   time.Time
}

type GetAllBlockedApplsParams struct {
	Limit     int32
	Page      int32
	CompanyID int32
}

type GetAllBlockedApplsRes struct {
	BlockedAppls []*BlockedApplicant
	Count        int32
}
