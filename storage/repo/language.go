package repo

type Language struct {
	ID        int64
	VacancyID int64
	Language  string
	Level     string
}

type GetAllLanguagesParams struct {
	Limit  int32
	Page   int32
	VacancyID int64
}

type GetAllLanguagesResult struct {
	Languages []*Language
	Count     int32
}

type LanguageStorageI interface {
	Create(l *Language) (*Language, error)
	GetAll(params *GetAllLanguagesParams) (*GetAllLanguagesResult, error)
	Delete(VacancyID int64) error
}
