package repo

import "time"

type VacancyStorageI interface {
	Create(c *Vacancy) (*Vacancy, error)
	Get(id int64) (*Vacancy, error)
	JustGet(id int64) (*Vacancy, error)
	GetAllActive(params *GetAllVacanciesParams) (*GetAllVacanciesRes, error)
	GetAllDisactive(params *GetAllDisactiveParams) (*GetAllVacanciesRes, error)
	Update(c *Vacancy) (*Vacancy, error)
	Delete(id int64, companyID int64) error
	ActivateOrDisactivate(Id, CompanyID int64, smth bool) (*Vacancy, error)
	AddView(id int64) error
	GetSlug(slug string) (int64, error)
}

type Vacancy struct {
	ID             int64
	CompanyID      int64
	Title          string
	Deadline       string
	EmploymentForm string
	EmploymentType string
	Country        string
	City           string
	SalaryMin      float64
	SalaryMax      float64
	Currency       string
	SalaryPeriod   string
	Education      string
	Experience     int64
	Description    string
	IsActive       bool
	ViewCount      int64
	Slug           string
	CreatedAt      time.Time
	UpdatedAt      time.Time
}

type GetAllVacanciesParams struct {
	Limit             int64
	Page              int64
	CompanyID         int64
	Title             string
	Market            string
	EmploymentForm    string
	EmploymentType    string
	Location          string
	TimeRange         string
	YearsOfExperience int64
}

type GetAllDisactiveParams struct {
	Limit     int64
	Page      int64
	CompanyID int64
}

type GetAllVacanciesRes struct {
	Vacancies []*Vacancy
	Count     int32
}
