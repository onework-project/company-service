package repo

import "time"

type CompanyMemberStorageI interface {
	AddMember(m *Member) (*Member, error)
	GetMember(id int64) (*Member, error)
	ExchangeMemberType(ex *ExchangeType) error
	DeleteMember(memberId int64) error
	GetAllMembers(params *GetAllMembersParams) (*GetAllMembers, error)
}

type ExchangeType struct {
	OwnerID int64
	AdminID int64
}

type Member struct {
	ID        int64
	CompanyID int64
	MemberID  int64
	UserType  string
	CreatedAt time.Time
}

type GetAllMembersParams struct {
	Limit     int64
	Page      int64
	CompanyID int64
}

type GetAllMembers struct {
	Members []*Member
	Count   int64
}
