package repo

import "time"

type CompanyStorageI interface {
	Create(c *Company) (*Company, error)
	Get(id int64) (*Company, error)
	GetAll(params *GetAllCompaniesParams) (*GetAllCompaniesResult, error)
	Update(c *Company) (*Company, error)
	Delete(id int64) error
	GetSlug(slug string) (int64, error)
}

type Company struct {
	ID           int64
	CompanyName  string
	Description  string
	Email        string
	PhoneNumber  string
	Country      string
	RegionState  string
	WorkersCount string
	ImageUrl     string
	Posts        int64
	Slug         string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    time.Time
}

type GetAllCompaniesParams struct {
	Limit       int32
	Page        int32
	CompanyName string
	Location    string
}

type GetAllCompaniesResult struct {
	Companies []*Company
	Count     int32
}
