package repo

type SMStorageI interface {
	Create(sm *SocialMedia) (*SocialMedia, error)
	Delete(CompanyID int64) error
	GetAllSMs(params *GetAllSMsParams) (*GetAllSMsRes, error)
}

type SocialMedia struct {
	ID        int64
	CompanyID int64
	Name      string
	Url       string
}

type GetAllSMsParams struct {
	Limit     int32
	Page      int32
	CompanyID int64
}

type GetAllSMsRes struct {
	SMs   []*SocialMedia
	Count int32
}
