package repo

import "time"

type Application struct {
	ID              int64
	VacancyID       int64
	ApplicantID     int64
	Status          string
	RejectionReason string
	CreatedAt       time.Time
}

type GetAllApplicationsParams struct {
	Limit       int32
	Page        int32
	VacancyID   int64
	ApplicantID int64
}

type GetAllApplicationsResult struct {
	Applications []*Application
	Count        int32
}

type ApplicationStorageI interface {
	Create(c *Application) (*Application, error)
	Get(id int64) (*Application, error)
	GetAll(params *GetAllApplicationsParams) (*GetAllApplicationsResult, error)
	GetAllAccordingToStatus(status string) (*GetAllApplicationsResult, error)
	UpdateStatus(c *Application) (*Application, error)
	UpdateStatusRejection(c *Application) (*Application, error)
	Delete(id, vacancyID int64) error
	AutoDelete(id int64) error
}
