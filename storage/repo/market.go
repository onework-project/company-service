package repo

type MarketStorageI interface {
	Create(m *Market) (*Market, error)
	Delete(vacancyID int64) error
	GetAll(params *GetAllMarketsParams) (*GetAllMarketsRes, error)
}

type Market struct {
	ID        int64
	VacancyID int64
	Market    string
}

type GetAllMarketsParams struct {
	Limit     int32
	Page      int32
	VacancyID int64
}

type GetAllMarketsRes struct {
	Markets []*Market
	Count   int32
}
