package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

type Config struct {
	GrpcPort string
	Postgres PostgresConfig

	ApplicantServiceHost     string
	ApplicantServiceGrpcPort string
	AuthServiceHost          string
	AuthServiceGrpcPort      string
}

type PostgresConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

func Load(path string) Config {
	godotenv.Load(path + "/.env")

	conf := viper.New()
	conf.AutomaticEnv()

	cfg := Config{
		GrpcPort: conf.GetString("GRPC_PORT"),
		Postgres: PostgresConfig{
			Host:     conf.GetString("POSTGRES_HOST"),
			Port:     conf.GetString("POSTGRES_PORT"),
			User:     conf.GetString("POSTGRES_USER"),
			Password: conf.GetString("POSTGRES_PASSWORD"),
			Database: conf.GetString("POSTGRES_DATABASE"),
		},
		ApplicantServiceHost:     conf.GetString("APPLICANT_SERVICE_HOST"),
		ApplicantServiceGrpcPort: conf.GetString("APPLICANT_SERVICE_GRPC_PORT"),
		AuthServiceHost:          conf.GetString("AUTH_SERVICE_HOST"),
		AuthServiceGrpcPort:      conf.GetString("AUTH_SERVICE_GRPC_PORT"),
	}
	return cfg
}
