package grpc_client

import (
	"fmt"

	"gitlab.com/onework-project/company-service/config"
	pba "gitlab.com/onework-project/company-service/genproto/applicant_service"
	pb "gitlab.com/onework-project/company-service/genproto/auth_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type GrpcClientI interface {
	ApplicantService() pba.ApplicantServiceClient
	UserService() pb.UserServiceClient
}

type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (GrpcClientI, error) {
	connApplicantService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("company service dial host:%s port:%s err: %v",
			cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort, err)
	}
	connUserService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.AuthServiceHost, cfg.AuthServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("company service dial host:%s port:%s err: %v",
			cfg.AuthServiceHost, cfg.AuthServiceGrpcPort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"applicant_service": pba.NewApplicantServiceClient(connApplicantService),
			"user_service":      pb.NewUserServiceClient(connUserService),
		},
	}, nil

}

func (g *GrpcClient) ApplicantService() pba.ApplicantServiceClient {
	return g.connections["applicant_service"].(pba.ApplicantServiceClient)
}

func (g *GrpcClient) UserService() pb.UserServiceClient {
	return g.connections["user_service"].(pb.UserServiceClient)
}
