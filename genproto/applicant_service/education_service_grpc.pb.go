// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.12
// source: education_service.proto

package applicant_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// EducationServiceClient is the client API for EducationService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type EducationServiceClient interface {
	Create(ctx context.Context, in *Education, opts ...grpc.CallOption) (*Education, error)
	Get(ctx context.Context, in *GetReq, opts ...grpc.CallOption) (*Education, error)
	Update(ctx context.Context, in *Education, opts ...grpc.CallOption) (*Education, error)
	Delete(ctx context.Context, in *DeleteOrGetReq, opts ...grpc.CallOption) (*Empty, error)
	GetAll(ctx context.Context, in *GetAllParams, opts ...grpc.CallOption) (*GetAllEdus, error)
}

type educationServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewEducationServiceClient(cc grpc.ClientConnInterface) EducationServiceClient {
	return &educationServiceClient{cc}
}

func (c *educationServiceClient) Create(ctx context.Context, in *Education, opts ...grpc.CallOption) (*Education, error) {
	out := new(Education)
	err := c.cc.Invoke(ctx, "/genproto.EducationService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *educationServiceClient) Get(ctx context.Context, in *GetReq, opts ...grpc.CallOption) (*Education, error) {
	out := new(Education)
	err := c.cc.Invoke(ctx, "/genproto.EducationService/Get", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *educationServiceClient) Update(ctx context.Context, in *Education, opts ...grpc.CallOption) (*Education, error) {
	out := new(Education)
	err := c.cc.Invoke(ctx, "/genproto.EducationService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *educationServiceClient) Delete(ctx context.Context, in *DeleteOrGetReq, opts ...grpc.CallOption) (*Empty, error) {
	out := new(Empty)
	err := c.cc.Invoke(ctx, "/genproto.EducationService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *educationServiceClient) GetAll(ctx context.Context, in *GetAllParams, opts ...grpc.CallOption) (*GetAllEdus, error) {
	out := new(GetAllEdus)
	err := c.cc.Invoke(ctx, "/genproto.EducationService/GetAll", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// EducationServiceServer is the server API for EducationService service.
// All implementations must embed UnimplementedEducationServiceServer
// for forward compatibility
type EducationServiceServer interface {
	Create(context.Context, *Education) (*Education, error)
	Get(context.Context, *GetReq) (*Education, error)
	Update(context.Context, *Education) (*Education, error)
	Delete(context.Context, *DeleteOrGetReq) (*Empty, error)
	GetAll(context.Context, *GetAllParams) (*GetAllEdus, error)
	mustEmbedUnimplementedEducationServiceServer()
}

// UnimplementedEducationServiceServer must be embedded to have forward compatible implementations.
type UnimplementedEducationServiceServer struct {
}

func (UnimplementedEducationServiceServer) Create(context.Context, *Education) (*Education, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedEducationServiceServer) Get(context.Context, *GetReq) (*Education, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Get not implemented")
}
func (UnimplementedEducationServiceServer) Update(context.Context, *Education) (*Education, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedEducationServiceServer) Delete(context.Context, *DeleteOrGetReq) (*Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedEducationServiceServer) GetAll(context.Context, *GetAllParams) (*GetAllEdus, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAll not implemented")
}
func (UnimplementedEducationServiceServer) mustEmbedUnimplementedEducationServiceServer() {}

// UnsafeEducationServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to EducationServiceServer will
// result in compilation errors.
type UnsafeEducationServiceServer interface {
	mustEmbedUnimplementedEducationServiceServer()
}

func RegisterEducationServiceServer(s grpc.ServiceRegistrar, srv EducationServiceServer) {
	s.RegisterService(&EducationService_ServiceDesc, srv)
}

func _EducationService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Education)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EducationServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.EducationService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EducationServiceServer).Create(ctx, req.(*Education))
	}
	return interceptor(ctx, in, info, handler)
}

func _EducationService_Get_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EducationServiceServer).Get(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.EducationService/Get",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EducationServiceServer).Get(ctx, req.(*GetReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _EducationService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Education)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EducationServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.EducationService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EducationServiceServer).Update(ctx, req.(*Education))
	}
	return interceptor(ctx, in, info, handler)
}

func _EducationService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteOrGetReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EducationServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.EducationService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EducationServiceServer).Delete(ctx, req.(*DeleteOrGetReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _EducationService_GetAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAllParams)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EducationServiceServer).GetAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.EducationService/GetAll",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EducationServiceServer).GetAll(ctx, req.(*GetAllParams))
	}
	return interceptor(ctx, in, info, handler)
}

// EducationService_ServiceDesc is the grpc.ServiceDesc for EducationService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var EducationService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "genproto.EducationService",
	HandlerType: (*EducationServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _EducationService_Create_Handler,
		},
		{
			MethodName: "Get",
			Handler:    _EducationService_Get_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _EducationService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _EducationService_Delete_Handler,
		},
		{
			MethodName: "GetAll",
			Handler:    _EducationService_GetAll_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "education_service.proto",
}
