package service

import (
	"context"
	"database/sql"
	"errors"
	"sync"
	"time"

	"gitlab.com/onework-project/company-service/genproto/applicant_service"
	pb "gitlab.com/onework-project/company-service/genproto/company_service"
	grpcPkg "gitlab.com/onework-project/company-service/pkg/grpc_client"
	"gitlab.com/onework-project/company-service/pkg/logger"
	"gitlab.com/onework-project/company-service/storage"
	"gitlab.com/onework-project/company-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ApplicationService struct {
	pb.UnimplementedApplicationServiceServer
	grpcClient grpcPkg.GrpcClientI
	storage    storage.StorageI
	logger     *logger.Logger
}

func NewApplicationService(strg storage.StorageI, logger *logger.Logger, grpcConn grpcPkg.GrpcClientI) *ApplicationService {
	return &ApplicationService{
		storage:    strg,
		logger:     logger,
		grpcClient: grpcConn,
	}
}

func (s *ApplicationService) Create(ctx context.Context, req *pb.Application) (*pb.Application, error) {
	resp, err := s.storage.Application().Create(&repo.Application{
		VacancyID:   req.VacancyId,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create application")
		return nil, status.Errorf(codes.Internal, "failed to create application: %v", err)
	}

	applicant, vacancy, company := s.getTwoInfo(req.ApplicantId, req.VacancyId)

	return parseModels(resp, vacancy, applicant, company), nil
}

func (s *ApplicationService) getTwoInfo(applicantId, vacancyId int64) (*applicant_service.Applicant, *repo.Vacancy, *repo.Company) {
	var wg sync.WaitGroup
	var err error
	var applicant *applicant_service.Applicant
	var vacancy *repo.Vacancy
	var companyInfo *repo.Company
	wg.Add(2)
	go func() {
		defer wg.Done()
		applicant, err = s.grpcClient.ApplicantService().Get(context.Background(), &applicant_service.IdRequest{Id: applicantId})
		if err != nil {
			s.logger.WithError(err).Error("failed to get applicant info")
		}
	}()
	go func() {
		defer wg.Done()
		vacancy, err = s.storage.Vacancy().JustGet(vacancyId)
		if err != nil {
			s.logger.WithError(err).Error("failed to get vacancy info")
		} else {
			companyInfo, err = s.storage.Company().Get(vacancy.CompanyID)
			if err != nil {
				s.logger.WithError(err).Error("failed to get company info")
			}
		}
	}()

	wg.Wait()
	return applicant, vacancy, companyInfo
}

func (s *ApplicationService) Get(ctx context.Context, req *pb.GetOrDeleteApplicationReq) (*pb.Application, error) {
	resp, err := s.storage.Application().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to get application")
		return nil, status.Errorf(codes.Internal, "failed to get an application: %v", err)
	}
	applicant, vacancy, company := s.getTwoInfo(resp.ApplicantID, resp.VacancyID)

	return parseModels(resp, vacancy, applicant, company), nil
}

func (s *ApplicationService) GetAll(ctx context.Context, req *pb.GetAllApplicationParams) (*pb.GetAllApplicationsRes, error) {
	ap, err := s.storage.Application().GetAll(&repo.GetAllApplicationsParams{
		Limit:       req.Limit,
		Page:        req.Page,
		VacancyID:   req.VacancyId,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get all applications")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get all applications: %v", err)
	}
	response := pb.GetAllApplicationsRes{
		Applications: make([]*pb.Application, 0),
		Count:        ap.Count,
	}
	for _, ap := range ap.Applications {
		applicant, vacancy, company := s.getTwoInfo(ap.ApplicantID, ap.VacancyID)
		response.Applications = append(response.Applications, parseModels(ap, vacancy, applicant, company))
	}

	return &response, nil
}

func (s *ApplicationService) UpdateStatus(ctx context.Context, req *pb.ApplicationUpdateStatus) (*pb.Application, error) {
	resp, err := s.storage.Application().UpdateStatus(&repo.Application{
		Status:    req.Status,
		ID:        req.Id,
		VacancyID: req.VacancyId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update the status of application")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to update the status of application; %v", err)
	}
	applicant, vacancy, company := s.getTwoInfo(resp.ApplicantID, resp.VacancyID)
	return parseModels(resp, vacancy, applicant, company), nil
}

func (s *ApplicationService) UpdateStatusRejection(ctx context.Context, req *pb.RejectApplicationReq) (*pb.Application, error) {
	resp, err := s.storage.Application().UpdateStatusRejection(&repo.Application{
		RejectionReason: req.RejectionReason,
		ID:              req.Id,
		VacancyID:       req.VacancyId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update the status of application")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to update the status of application; %v", err)
	}
	applicant, vacancy, company := s.getTwoInfo(resp.ApplicantID, resp.VacancyID)
	return parseModels(resp, vacancy, applicant, company), nil
}

func (s *ApplicationService) Delete(ctx context.Context, req *pb.ApplicationIDReq) (*emptypb.Empty, error) {
	err := s.storage.Application().Delete(req.Id, req.VacancyId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete application")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to delete application: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *ApplicationService) AutoDelete(ctx context.Context, req *pb.GetOrDeleteApplicationReq) (*emptypb.Empty, error) {
	err := s.storage.Application().AutoDelete(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete application")
		return nil, status.Errorf(codes.Internal, "failed to delete application: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *ApplicationService) GetAllAccordingToStatus(ctx context.Context, req *pb.ApplicationStatus) (*pb.GetAllApplicationsRes, error) {
	ap, err := s.storage.Application().GetAllAccordingToStatus(req.Status)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to get all applications: %v", err)
	}
	response := pb.GetAllApplicationsRes{
		Applications: make([]*pb.Application, 0),
		Count:        ap.Count,
	}
	for _, ap := range ap.Applications {
		applicant, vacancy, company := s.getTwoInfo(ap.ApplicantID, ap.VacancyID)
		response.Applications = append(response.Applications, parseModels(ap, vacancy, applicant, company))
	}

	return &response, nil
}

func parseModels(ap *repo.Application, v *repo.Vacancy, a *applicant_service.Applicant, company *repo.Company) *pb.Application {
	return &pb.Application{
		Id:              ap.ID,
		VacancyId:       ap.VacancyID,
		ApplicantId:     ap.ApplicantID,
		Status:          ap.Status,
		RejectionReason: ap.RejectionReason,
		CreatedAt:       ap.CreatedAt.Format(time.RFC3339),
		Vacancy: &pb.VacancyData{
			Id:        v.ID,
			CompanyId: v.CompanyID,
			CompanyInfo: &pb.Company{
				Id:           company.ID,
				CompanyName:  company.CompanyName,
				Description:  company.Description,
				Email:        company.Email,
				PhoneNumber:  company.PhoneNumber,
				Country:      company.Country,
				RegionState:  company.RegionState,
				WorkersCount: company.WorkersCount,
				ImageUrl:     company.ImageUrl,
				Posts:        company.Posts,
				CreatedAt:    company.CreatedAt.Format(time.RFC3339),
				UpdatedAt:    company.UpdatedAt.Format(time.RFC3339),
				Slug:         company.Slug,
			},
			Title:          v.Title,
			Deadline:       v.Deadline,
			EmploymentForm: v.EmploymentForm,
			EmploymentType: v.EmploymentType,
			Country:        v.Country,
			City:           v.City,
			SalaryMin:      float32(v.SalaryMin),
			SalaryMax:      float32(v.SalaryMax),
			Currency:       v.Currency,
			SalaryPeriod:   v.SalaryPeriod,
			Education:      v.Education,
			Experience:     v.Experience,
			Description:    v.Description,
			ViewsCount:     v.ViewCount,
			IsActive:       v.IsActive,
			Slug:           v.Slug,
		},
		Applicant: &pb.ApplicantData{
			Id:           a.Id,
			FirstName:    a.FirstName,
			LastName:     a.LastName,
			Speciality:   a.Speciality,
			Country:      a.Country,
			City:         a.City,
			DateOfBirth:  a.DateOfBirth,
			InvisibleAge: a.InvisibleAge,
			PhoneNumber:  a.PhoneNumber,
			Email:        a.Email,
			About:        a.About,
			ImageUrl:     a.ImageUrl,
			Slug:         a.Slug,
		},
	}
}
