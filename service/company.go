package service

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/gosimple/slug"
	pb "gitlab.com/onework-project/company-service/genproto/company_service"
	"gitlab.com/onework-project/company-service/pkg/logger"
	"gitlab.com/onework-project/company-service/storage"
	"gitlab.com/onework-project/company-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type CompanyService struct {
	pb.UnimplementedCompanyServiceServer
	storage storage.StorageI
	logger  *logger.Logger
}

func NewCompanyService(strg storage.StorageI, logger *logger.Logger) *CompanyService {
	return &CompanyService{
		storage: strg,
		logger:  logger,
	}
}

func (s *CompanyService) Create(ctx context.Context, req *pb.CreateCompanyReq) (*pb.Company, error) {
	slugIs := slug.Make(req.CompanyName)
	id, err := s.storage.Company().GetSlug(slugIs)
	if !errors.Is(err, sql.ErrNoRows) && id != 0 {
		timestamp := time.Now().UnixNano() / int64(time.Millisecond)
		slugIs = slug.Make(fmt.Sprintf("%s %d", req.CompanyName, timestamp))
	}
	resp, err := s.storage.Company().Create(&repo.Company{
		CompanyName:  req.CompanyName,
		Description:  req.Description,
		Email:        req.Email,
		PhoneNumber:  req.PhoneNumber,
		Country:      req.Country,
		RegionState:  req.RegionState,
		WorkersCount: req.WorkersCount,
		ImageUrl:     req.ImageUrl,
		Slug:         slugIs,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create company")
		return nil, status.Errorf(codes.Internal, "failed to create company")
	}

	return parseCompanyModel(resp), nil
}

func (s *CompanyService) Get(ctx context.Context, req *pb.CompanyIDReq) (*pb.Company, error) {
	resp, err := s.storage.Company().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Errorf("failed to get company: %v", err)
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get company")
	}

	return parseCompanyModel(resp), nil
}

func (s *CompanyService) GetAll(ctx context.Context, req *pb.GetAllCompaniesParams) (*pb.GetAllCompaniesRes, error) {
	resp, err := s.storage.Company().GetAll(&repo.GetAllCompaniesParams{
		Limit:       req.Limit,
		Page:        req.Page,
		CompanyName: req.CompanyName,
		Location:    req.Location,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get companies")
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get all companies")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get all companies: %v", err)
	}
	response := pb.GetAllCompaniesRes{
		Companies: make([]*pb.Company, 0),
		Count:     resp.Count,
	}
	for _, v := range resp.Companies {
		response.Companies = append(response.Companies, parseCompanyModel(v))
	}

	return &response, nil
}

func (s *CompanyService) Update(ctx context.Context, req *pb.Company) (*pb.Company, error) {
	slugIs := slug.Make(req.CompanyName)
	id, err := s.storage.Company().GetSlug(slugIs)
	if !errors.Is(err, sql.ErrNoRows) && id != req.Id {
		timestamp := time.Now().UnixNano() / int64(time.Millisecond)
		slugIs = slug.Make(fmt.Sprintf("%s %d", req.CompanyName, timestamp))
	}
	resp, err := s.storage.Company().Update(&repo.Company{
		ID:           req.Id,
		Email:        req.Email,
		CompanyName:  req.CompanyName,
		Description:  req.Description,
		PhoneNumber:  req.PhoneNumber,
		Country:      req.Country,
		RegionState:  req.RegionState,
		WorkersCount: req.WorkersCount,
		ImageUrl:     req.ImageUrl,
		Slug:         slugIs,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update company")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to update company")
	}

	return parseCompanyModel(resp), nil
}

func (s *CompanyService) Delete(ctx context.Context, req *pb.CompanyIDReq) (*emptypb.Empty, error) {
	err := s.storage.Company().Delete(req.Id)
	if err != nil {
		s.logger.WithError(err).Errorf("failed to delete compny: %v", err)
		return nil, status.Errorf(codes.Internal, "failed to delete company")
	}

	return &emptypb.Empty{}, nil
}

func parseCompanyModel(c *repo.Company) *pb.Company {
	return &pb.Company{
		Id:           c.ID,
		CompanyName:  c.CompanyName,
		Description:  c.Description,
		Email:        c.Email,
		PhoneNumber:  c.PhoneNumber,
		Country:      c.Country,
		RegionState:  c.RegionState,
		WorkersCount: c.WorkersCount,
		ImageUrl:     c.ImageUrl,
		Posts:        c.Posts,
		Slug:         c.Slug,
		CreatedAt:    c.CreatedAt.Format(time.RFC3339),
		UpdatedAt:    c.UpdatedAt.Format(time.RFC3339),
	}
}
