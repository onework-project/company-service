package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/onework-project/company-service/genproto/company_service"
	"gitlab.com/onework-project/company-service/pkg/logger"
	"gitlab.com/onework-project/company-service/storage"
	"gitlab.com/onework-project/company-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SMService struct {
	pb.UnimplementedSMServiceServer
	storage storage.StorageI
	logger  *logger.Logger
}

func NewSMService(strg storage.StorageI, logger *logger.Logger) *SMService {
	return &SMService{
		storage: strg,
		logger:  logger,
	}
}

func (s *SMService) Create(ctx context.Context, req *pb.CompanySocialMedias) (*pb.CompanySocialMedias, error) {
	var (
		socialMedias pb.CompanySocialMedias
	)

	err := s.storage.SocialMedia().Delete(req.SocialMedias[0].CompanyId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete all social medias")
	}

	for _, v := range req.SocialMedias {
		socialMedia, err := s.storage.SocialMedia().Create(&repo.SocialMedia{
			CompanyID: v.CompanyId,
			Name:      v.Name,
			Url:       v.Url,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to create new language")
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
		socialMedias.SocialMedias = append(socialMedias.SocialMedias, &pb.ComSocialMedia{
			Id:        socialMedia.ID,
			CompanyId: socialMedia.CompanyID,
			Name:      socialMedia.Name,
			Url:       socialMedia.Url,
		})
	}

	return &socialMedias, nil
}

func (s *SMService) GetAll(ctx context.Context, req *pb.GetAllSMsParams) (*pb.GetAllSMsRes, error) {
	resp, err := s.storage.SocialMedia().GetAllSMs(&repo.GetAllSMsParams{
		Limit:     req.Limit,
		Page:      req.Page,
		CompanyID: req.CompanyId,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get all social medias")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get all social medias: %v", err)
	}
	response := pb.GetAllSMsRes{
		SocialMedias: make([]*pb.ComSocialMedia, resp.Count),
		Count:        resp.Count,
	}
	for i, v := range resp.SMs {
		response.SocialMedias[i] = parseSmModel(v)
	}

	return &response, nil
}

func parseSmModel(sm *repo.SocialMedia) *pb.ComSocialMedia {
	return &pb.ComSocialMedia{
		Id:        sm.ID,
		CompanyId: sm.CompanyID,
		Name:      sm.Name,
		Url:       sm.Url,
	}
}
