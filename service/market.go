package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/onework-project/company-service/genproto/company_service"
	"gitlab.com/onework-project/company-service/pkg/logger"
	"gitlab.com/onework-project/company-service/storage"
	"gitlab.com/onework-project/company-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type MarketService struct {
	pb.UnimplementedMarketServiceServer
	storage storage.StorageI
	logger  *logger.Logger
}

func NewMarketService(strg storage.StorageI, logger *logger.Logger) *MarketService {
	return &MarketService{
		storage: strg,
		logger:  logger,
	}
}

func (s *MarketService) Create(ctx context.Context, req *pb.VacancyMarkets) (*pb.VacancyMarkets, error) {
	var (
		markets pb.VacancyMarkets
	)

	err := s.storage.Market().Delete(req.Markets[0].VacancyId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete all markets")
	}

	for _, v := range req.Markets {
		market, err := s.storage.Market().Create(&repo.Market{
			VacancyID: v.VacancyId,
			Market:    v.Market,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to create new market")
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
		markets.Markets = append(markets.Markets, &pb.Market{
			Id:        market.ID,
			VacancyId: v.VacancyId,
			Market:    market.Market,
		})
	}

	return &markets, nil
}

func (s *MarketService) GetAll(ctx context.Context, req *pb.GetAllMarketsParams) (*pb.GetAllMarketsRes, error) {
	resp, err := s.storage.Market().GetAll(&repo.GetAllMarketsParams{
		Limit:     req.Limit,
		Page:      req.Page,
		VacancyID: req.VacancyId,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get all markets")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get all markets: %v", err)
	}
	response := pb.GetAllMarketsRes{
		Markets: make([]*pb.Market, resp.Count),
		Count:   resp.Count,
	}
	for i, v := range resp.Markets {
		response.Markets[i] = parseMarketModel(v)
	}

	return &response, nil
}

func parseMarketModel(v *repo.Market) *pb.Market {
	return &pb.Market{
		Id:        v.ID,
		VacancyId: v.VacancyID,
		Market:    v.Market,
	}
}
