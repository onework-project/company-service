package service

import (
	"context"
	"time"

	"gitlab.com/onework-project/company-service/genproto/applicant_service"
	pb "gitlab.com/onework-project/company-service/genproto/company_service"
	"gitlab.com/onework-project/company-service/pkg/grpc_client"
	"gitlab.com/onework-project/company-service/pkg/logger"
	"gitlab.com/onework-project/company-service/storage"
	"gitlab.com/onework-project/company-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type BlockedApplicationService struct {
	pb.UnimplementedBlockedApplicantServiceServer
	storage    storage.StorageI
	logger     *logger.Logger
	grpcClient grpc_client.GrpcClientI
}

func NewBlockedApplService(strg storage.StorageI, logger *logger.Logger, grpc grpc_client.GrpcClientI) *BlockedApplicationService {
	return &BlockedApplicationService{
		storage:    strg,
		logger:     logger,
		grpcClient: grpc,
	}
}

func (s *BlockedApplicationService) Create(ctx context.Context, req *pb.BlockedApplicant) (*pb.BlockedApplicant, error) {
	resp, err := s.storage.BlockedApplicant().Create(&repo.BlockedApplicant{
		CompanyID:   req.CompanyId,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to create blocked applicant: %v", err)
	}
	info, err := s.grpcClient.ApplicantService().Get(context.Background(), &applicant_service.IdRequest{
		Id: resp.ApplicantID,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get applicant info")
	}

	return parseBlockedApplModel(resp, info), nil
}

func (s *BlockedApplicationService) Delete(ctx context.Context, req *pb.BlockedApplIDReq) (*emptypb.Empty, error) {
	err := s.storage.BlockedApplicant().Delete(req.Id, req.CompanyId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete blocked applicant")
		return nil, status.Errorf(codes.Internal, "failed to delete blocked applicant: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *BlockedApplicationService) AutoDelete(ctx context.Context, req *pb.DeleteBlockedApp) (*emptypb.Empty, error) {
	err := s.storage.BlockedApplicant().AutoDelete(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete blocked applicant")
		return nil, status.Errorf(codes.Internal, "failed to delete blocked applicant: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *BlockedApplicationService) GetAll(ctx context.Context, req *pb.GetAllBlockedApplsParams) (*pb.GetAllBlockedApplsRes, error) {
	resp, err := s.storage.BlockedApplicant().GetAll(&repo.GetAllBlockedApplsParams{
		Limit:     req.Limit,
		Page:      req.Page,
		CompanyID: req.CompanyId,
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to get all blocked applicants: %v", err)
	}
	response := pb.GetAllBlockedApplsRes{
		BlockedApplicants: make([]*pb.BlockedApplicant, resp.Count),
		Count:             resp.Count,
	}
	for i, v := range resp.BlockedAppls {
		info, err := s.grpcClient.ApplicantService().Get(context.Background(), &applicant_service.IdRequest{
			Id: v.ApplicantID,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to get applicant info")
		}

		response.BlockedApplicants[i] = parseBlockedApplModel(v, info)
	}

	return &response, nil
}

func parseBlockedApplModel(ba *repo.BlockedApplicant, info *applicant_service.Applicant) *pb.BlockedApplicant {
	return &pb.BlockedApplicant{
		Id:          ba.ID,
		CompanyId:   ba.CompanyID,
		ApplicantId: ba.ApplicantID,
		ApplicantInfo: &pb.ApplicantInfo{
			Id:           info.Id,
			FirstName:    info.FirstName,
			LastName:     info.LastName,
			Speciality:   info.Speciality,
			Country:      info.Country,
			City:         info.City,
			DateOfBirth:  info.DateOfBirth,
			InvisibleAge: info.InvisibleAge,
			PhoneNumber:  info.PhoneNumber,
			Email:        info.Email,
			About:        info.About,
			ImageUrl:     info.ImageUrl,
			LastStep:     info.LastStep,
			CreatedAt:    info.CreatedAt,
			UpdatedAt:    info.UpdatedAt,
			Slug:         info.Slug,
		},
		CreatedAt: ba.CreatedAt.Format(time.RFC3339),
	}
}
