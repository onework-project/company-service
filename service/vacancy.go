package service

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/gosimple/slug"
	pb "gitlab.com/onework-project/company-service/genproto/company_service"
	"gitlab.com/onework-project/company-service/pkg/logger"
	"gitlab.com/onework-project/company-service/storage"
	"gitlab.com/onework-project/company-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type VacancyService struct {
	pb.UnimplementedVacancyServiceServer
	storage storage.StorageI
	logger  *logger.Logger
}

func NewVacancyService(strg storage.StorageI, logger *logger.Logger) *VacancyService {
	return &VacancyService{
		storage: strg,
		logger:  logger,
	}
}

func (s *VacancyService) Create(ctx context.Context, req *pb.Vacancy) (*pb.Vacancy, error) {
	slugIs := slug.Make(req.Title)
	id, err := s.storage.Vacancy().GetSlug(slugIs)
	if !errors.Is(err, sql.ErrNoRows) && id != 0 {
		timestamp := time.Now().UnixNano() / int64(time.Millisecond)
		slugIs = slug.Make(fmt.Sprintf("%s %d", req.Title, timestamp))
	}
	resp, err := s.storage.Vacancy().Create(&repo.Vacancy{
		CompanyID:      req.CompanyId,
		Title:          req.Title,
		Deadline:       req.Deadline,
		EmploymentForm: req.EmploymentForm,
		EmploymentType: req.EmploymentType,
		Country:        req.Country,
		City:           req.City,
		SalaryMin:      float64(req.SalaryMin),
		SalaryMax:      float64(req.SalaryMax),
		Currency:       req.Currency,
		SalaryPeriod:   req.SalaryPeriod,
		Education:      req.Education,
		Experience:     req.Experience,
		Description:    req.Description,
		Slug:           slugIs,
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to create vacancy: %v", err)
	}
	info, err := s.storage.Company().Get(resp.CompanyID)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to get company info: %v", err)
	}

	return s.parseVacancyModel(resp, info), nil
}

func (s *VacancyService) Get(ctx context.Context, req *pb.GetVacancyReq) (*pb.Vacancy, error) {
	resp, err := s.storage.Vacancy().Get(req.Id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get vacancy")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get vacancy: %v", err)
	}
	companyInfo, err := s.storage.Company().Get(resp.CompanyID)
	if err != nil {
		s.logger.WithError(err).Error("failed to get company info")
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get company")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get company: %v", err)
	}
	return s.parseVacancyModel(resp, companyInfo), nil
}

func (s *VacancyService) Update(ctx context.Context, req *pb.Vacancy) (*pb.Vacancy, error) {
	slugIs := slug.Make(req.Title)
	id, err := s.storage.Vacancy().GetSlug(slugIs)
	if !errors.Is(err, sql.ErrNoRows) && id != req.Id {
		timestamp := time.Now().UnixNano() / int64(time.Millisecond)
		slugIs = slug.Make(fmt.Sprintf("%s %d", req.Title, timestamp))
	}
	resp, err := s.storage.Vacancy().Update(&repo.Vacancy{
		ID:             req.Id,
		CompanyID:      req.CompanyId,
		Title:          req.Title,
		Deadline:       req.Deadline,
		EmploymentForm: req.EmploymentForm,
		EmploymentType: req.EmploymentType,
		Country:        req.Country,
		City:           req.City,
		SalaryMin:      float64(req.SalaryMin),
		SalaryMax:      float64(req.SalaryMax),
		Currency:       req.Currency,
		SalaryPeriod:   req.SalaryPeriod,
		Education:      req.Education,
		Experience:     req.Experience,
		Description:    req.Description,
		Slug:           slugIs,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update vacancy")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to create vacancy: %v", err)
	}
	info, err := s.storage.Company().Get(resp.CompanyID)
	if err != nil {
		s.logger.WithError(err).Error("failed to get company info")
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get company")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get company info: %v", err)
	}

	return s.parseVacancyModel(resp, info), nil
}

func (s *VacancyService) Delete(ctx context.Context, req *pb.VacancyIDReq) (*emptypb.Empty, error) {
	err := s.storage.Vacancy().Delete(req.Id, req.CompanyId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete vacancy")
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to delete vacancy")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to delete vacancy: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s VacancyService) ActivateOrDisActivate(ctx context.Context, req *pb.ActivateOrDisactivateReq) (*pb.Vacancy, error) {
	vacancy, err := s.storage.Vacancy().ActivateOrDisactivate(req.Id, req.CompanyId, req.Smth)
	if err != nil {
		s.logger.WithError(err).Error("failed to activate or disactivate: %v", err)
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get vacancy")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get vacancy: %v", err)
	}
	info, err := s.storage.Company().Get(vacancy.CompanyID)
	if err != nil {
		s.logger.WithError(err).Error("failed to get info og company")
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get company info")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get vacancy: %v", err)
	}

	return s.parseVacancyModel(vacancy, info), nil
}

func (s *VacancyService) GetAllActive(ctx context.Context, req *pb.GetAllVacanciesParams) (*pb.GetAllVacanciesRes, error) {
	resp, err := s.storage.Vacancy().GetAllActive(&repo.GetAllVacanciesParams{
		Limit:             int64(req.Limit),
		Page:              int64(req.Page),
		CompanyID:         req.CompanyId,
		Title:             req.Title,
		Market:            req.Market,
		EmploymentForm:    req.EmploymentForm,
		EmploymentType:    req.EmploymentType,
		Location:          req.Location,
		TimeRange:         req.TimeRange,
		YearsOfExperience: req.YearsOfExperience,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get vacancy")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get vacancy: %v", err)
	}
	response := pb.GetAllVacanciesRes{
		Vacancies: make([]*pb.Vacancy, 0),
		Count:     resp.Count,
	}
	for _, v := range resp.Vacancies {
		companyInfo, err := s.storage.Company().Get(v.CompanyID)
		if err != nil {
			s.logger.WithError(err).Error("failed to get company info")
			if errors.Is(err, sql.ErrNoRows) {
				s.logger.WithError(err).Error("failed to get company")
				return nil, status.Errorf(codes.NotFound, err.Error())
			}
			return nil, status.Errorf(codes.Internal, "failed to get company: %v", err)
		}
		a := s.parseVacancyModel(v, companyInfo)
		response.Vacancies = append(response.Vacancies, a)
	}

	return &response, nil
}

func (s *VacancyService) AddView(ctx context.Context, req *pb.AddViewReq) (*emptypb.Empty, error) {
	err := s.storage.Vacancy().AddView(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to add a view to vacancy")
		return nil, status.Errorf(codes.Internal, "failed to add a view to vacancy")
	}

	return &emptypb.Empty{}, nil
}

func (s *VacancyService) GetAllDisactive(ctx context.Context, req *pb.GetAllDisactiveParams) (*pb.GetAllVacanciesRes, error) {
	resp, err := s.storage.Vacancy().GetAllDisactive(&repo.GetAllDisactiveParams{
		Limit:     req.Limit,
		Page:      req.Page,
		CompanyID: req.CompanyId,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get vacancy")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get vacancy: %v", err)
	}
	response := pb.GetAllVacanciesRes{
		Vacancies: make([]*pb.Vacancy, 0),
		Count:     resp.Count,
	}
	for _, v := range resp.Vacancies {
		companyInfo, err := s.storage.Company().Get(v.CompanyID)
		if err != nil {
			s.logger.WithError(err).Error("failed to get company info")
			if errors.Is(err, sql.ErrNoRows) {
				s.logger.WithError(err).Error("failed to get company")
				return nil, status.Errorf(codes.NotFound, err.Error())
			}
			return nil, status.Errorf(codes.Internal, "failed to get company: %v", err)
		}
		a := s.parseVacancyModel(v, companyInfo)
		response.Vacancies = append(response.Vacancies, a)
	}

	return &response, nil
}

func (s *VacancyService) parseVacancyModel(v *repo.Vacancy, info *repo.Company) *pb.Vacancy {
	vacancy := pb.Vacancy{
		Id:        v.ID,
		CompanyId: v.CompanyID,
		CompanyInfo: &pb.GetCompanyInfo{
			CompanyName:  info.CompanyName,
			Description:  info.Description,
			Email:        info.Email,
			PhoneNumber:  info.PhoneNumber,
			Country:      info.Country,
			RegionState:  info.RegionState,
			WorkersCount: info.WorkersCount,
			ImageUrl:     info.ImageUrl,
			CreatedAt:    info.CreatedAt.Format(time.RFC3339),
			UpdatedAt:    info.UpdatedAt.Format(time.RFC3339),
			Slug:         info.Slug,
		},
		Title:          v.Title,
		Deadline:       v.Deadline,
		EmploymentForm: v.EmploymentForm,
		EmploymentType: v.EmploymentType,
		Country:        v.Country,
		City:           v.City,
		SalaryMin:      float32(v.SalaryMin),
		SalaryMax:      float32(v.SalaryMax),
		Currency:       v.Currency,
		SalaryPeriod:   v.SalaryPeriod,
		Education:      v.Education,
		Experience:     v.Experience,
		Description:    v.Description,
		IsActive:       v.IsActive,
		ViewsCount:     v.ViewCount,
		Slug:           v.Slug,
		CreatedAt:      v.CreatedAt.Format(time.RFC3339),
		UpdatedAt:      v.UpdatedAt.Format(time.RFC3339),
	}
	s.getVacancyThingsInfo(&vacancy)
	return &vacancy
}

func (s *VacancyService) getVacancyThingsInfo(vacancy *pb.Vacancy) {
	var wg sync.WaitGroup
	wg.Add(3)
	go func() {
		defer wg.Done()
		skills, err := s.storage.Skill().GetAll(&repo.GetAllSkillsParams{
			Limit:     1000,
			Page:      1,
			VacancyID: vacancy.Id,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to get all skills")
		}
		for _, v := range skills.Skills {
			a := parseSkillModel(v)
			vacancy.Skills = append(vacancy.Skills, a)
		}
	}()
	go func() {
		defer wg.Done()
		markets, err := s.storage.Market().GetAll(&repo.GetAllMarketsParams{
			Limit:     1000,
			Page:      1,
			VacancyID: vacancy.Id,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to get all skills")
		}
		for _, v := range markets.Markets {
			a := parseMarketModel(v)
			vacancy.Markets = append(vacancy.Markets, a)
		}
	}()

	go func() {
		defer wg.Done()
		languages, err := s.storage.Language().GetAll(&repo.GetAllLanguagesParams{
			Limit:     1000,
			Page:      1,
			VacancyID: vacancy.Id,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to get all skills")
		}
		for _, v := range languages.Languages {
			a := parseLanguageModel(v)
			vacancy.Languages = append(vacancy.Languages, a)
		}
	}()
	wg.Wait()
}
