package service

import (
	"context"
	"database/sql"
	"errors"
	"sync"
	"time"

	pba "gitlab.com/onework-project/company-service/genproto/auth_service"
	pb "gitlab.com/onework-project/company-service/genproto/company_service"
	"gitlab.com/onework-project/company-service/pkg/grpc_client"
	"gitlab.com/onework-project/company-service/pkg/logger"
	"gitlab.com/onework-project/company-service/storage"
	"gitlab.com/onework-project/company-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type CompanyMemberService struct {
	pb.UnimplementedCompanyMemberServiceServer
	storage    storage.StorageI
	logger     *logger.Logger
	grpcClient grpc_client.GrpcClientI
}

func NewCompanyMemberService(strg storage.StorageI, logger *logger.Logger, grpcClient grpc_client.GrpcClientI) *CompanyMemberService {
	return &CompanyMemberService{
		storage:    strg,
		logger:     logger,
		grpcClient: grpcClient,
	}
}

func (s *CompanyMemberService) AddMember(ctx context.Context, req *pb.Member) (*pb.Member, error) {
	resp, err := s.storage.CompanyMember().AddMember(&repo.Member{
		CompanyID: req.CompanyId,
		MemberID:  req.MemberId,
		UserType:  req.UserType,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create company member")
		return nil, status.Errorf(codes.Internal, "failed to create company member")
	}
	memberInfo, companyInfo := s.getTwoInfo(req.CompanyId, req.MemberId)
	return parseCompanyMemberModel(resp, companyInfo, memberInfo), nil
}

func (s *CompanyMemberService) getTwoInfo(companyId, memberId int64) (*pba.User, *repo.Company) {
	var err error
	var wg sync.WaitGroup
	var companyInfo *repo.Company
	var memberInfo *pba.User
	wg.Add(2)
	go func() {
		defer wg.Done()
		memberInfo, err = s.grpcClient.UserService().Get(context.Background(), &pba.GetByIdRequest{
			Id: memberId,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to get member info")
		}
	}()
	go func() {
		defer wg.Done()
		companyInfo, err = s.storage.Company().Get(companyId)
		if err != nil {
			s.logger.WithError(err).Error("failed to get company info")
		}
	}()
	wg.Wait()

	return memberInfo, companyInfo
}

func (s *CompanyMemberService) GetMember(ctx context.Context, req *pb.MemberId) (*pb.Member, error) {
	resp, err := s.storage.CompanyMember().GetMember(req.Id)
	if err != nil {
		s.logger.WithError(err).Errorf("failed to get company: %v", err)
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get company")
	}
	memberInfo, companyInfo := s.getTwoInfo(resp.CompanyID, resp.MemberID)
	return parseCompanyMemberModel(resp, companyInfo, memberInfo), nil
}

func (s *CompanyMemberService) GetAllMembers(ctx context.Context, req *pb.GetAllMembersParams) (*pb.GetAllMembersRes, error) {
	resp, err := s.storage.CompanyMember().GetAllMembers(&repo.GetAllMembersParams{
		Limit:     int64(req.Limit),
		Page:      int64(req.Page),
		CompanyID: req.CompanyId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get company member")
		return nil, status.Errorf(codes.Internal, "failed to get all companies: %v", err)
	}
	response := pb.GetAllMembersRes{
		Members: make([]*pb.Member, 0),
		Count:   resp.Count,
	}
	for _, v := range resp.Members {
		memberInfo, companyInfo := s.getTwoInfo(v.CompanyID, v.MemberID)
		response.Members = append(response.Members, parseCompanyMemberModel(v, companyInfo, memberInfo))
	}

	return &response, nil
}

func (s *CompanyMemberService) ExchangeMemberType(ctx context.Context, req *pb.ExchangeType) (*emptypb.Empty, error) {
	err := s.storage.CompanyMember().ExchangeMemberType(&repo.ExchangeType{
		OwnerID: req.OwnerId,
		AdminID: req.AdminId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update company")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to update company")
	}
	return &emptypb.Empty{}, nil
}

func (s *CompanyMemberService) DeleteMember(ctx context.Context, req *pb.MemberId) (*emptypb.Empty, error) {
	err := s.storage.CompanyMember().DeleteMember(req.Id)
	if err != nil {
		s.logger.WithError(err).Errorf("failed to delete compny: %v", err)
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to delete company")
	}

	return &emptypb.Empty{}, nil
}

func parseCompanyMemberModel(m *repo.Member, c *repo.Company, member *pba.User) *pb.Member {
	return &pb.Member{
		Id:        m.ID,
		CompanyId: m.CompanyID,
		CompanyInfo: &pb.Company{
			Id:           c.ID,
			CompanyName:  c.CompanyName,
			Description:  c.Description,
			Email:        c.Email,
			PhoneNumber:  c.PhoneNumber,
			Country:      c.Country,
			RegionState:  c.RegionState,
			WorkersCount: c.WorkersCount,
			ImageUrl:     c.ImageUrl,
			CreatedAt:    c.CreatedAt.Format(time.RFC3339),
			UpdatedAt:    c.UpdatedAt.Format(time.RFC3339),
			Slug:         c.Slug,
		},
		MemberId: m.MemberID,
		MemberInfo: &pb.MemberInfo{
			Id:        member.Id,
			Email:     member.Email,
			Type:      member.Type,
			CreatedAt: member.CreatedAt,
		},
		UserType:  m.UserType,
		CreatedAt: m.CreatedAt.Format(time.RFC3339),
	}
}
