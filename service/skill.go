package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/onework-project/company-service/genproto/company_service"
	"gitlab.com/onework-project/company-service/pkg/logger"
	"gitlab.com/onework-project/company-service/storage"
	"gitlab.com/onework-project/company-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SkillService struct {
	pb.UnimplementedComSkillServiceServer
	storage storage.StorageI
	logger  *logger.Logger
}

func NewSkillService(strg storage.StorageI, logger *logger.Logger) *SkillService {
	return &SkillService{
		storage: strg,
		logger:  logger,
	}
}

func (s *SkillService) Create(ctx context.Context, req *pb.VacancySkills) (*pb.VacancySkills, error) {
	var (
		languages pb.VacancySkills
	)

	err := s.storage.Skill().Delete(req.Skills[0].VacancyId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete all skills")
	}

	for _, v := range req.Skills {
		language, err := s.storage.Skill().Create(&repo.Skill{
			VacancyID: v.VacancyId,
			Name:      v.Name,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to create new skill")
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
		languages.Skills = append(languages.Skills, &pb.ComSkill{
			Id:        language.ID,
			VacancyId: v.VacancyId,
			Name:      language.Name,
		})
	}

	return &languages, nil
}

func (s *SkillService) GetAll(ctx context.Context, req *pb.ComGetAllSkillsParams) (*pb.ComGetAllSkillsRes, error) {
	resp, err := s.storage.Skill().GetAll(&repo.GetAllSkillsParams{
		Limit:     req.Limit,
		Page:      req.Page,
		VacancyID: req.VacancyId,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get all skill")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get all skill: %v", err)
	}
	response := pb.ComGetAllSkillsRes{
		Skills: make([]*pb.ComSkill, resp.Count),
		Count:  resp.Count,
	}
	for i, v := range resp.Skills {
		response.Skills[i] = parseSkillModel(v)
	}

	return &response, nil
}

func parseSkillModel(s *repo.Skill) *pb.ComSkill {
	return &pb.ComSkill{
		Id:        s.ID,
		VacancyId: s.VacancyID,
		Name:      s.Name,
	}
}
