package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/onework-project/company-service/genproto/company_service"
	"gitlab.com/onework-project/company-service/pkg/logger"
	"gitlab.com/onework-project/company-service/storage"
	"gitlab.com/onework-project/company-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type LanguageService struct {
	pb.UnimplementedCLanguageServiceServer
	storage storage.StorageI
	logger  *logger.Logger
}

func NewLanguageService(strg storage.StorageI, logger *logger.Logger) *LanguageService {
	return &LanguageService{
		storage: strg,
		logger:  logger,
	}
}

func (s *LanguageService) Create(ctx context.Context, req *pb.CompanyLanguages) (*pb.CompanyLanguages, error) {
	var (
		languages pb.CompanyLanguages
	)

	err := s.storage.Language().Delete(req.Languages[0].VacancyId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete all languages")
	}

	for _, v := range req.Languages {
		language, err := s.storage.Language().Create(&repo.Language{
			VacancyID: v.VacancyId,
			Language:  v.Language,
			Level:     v.Level,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to create new language")
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
		languages.Languages = append(languages.Languages, &pb.CLanguage{
			Id:        language.ID,
			VacancyId: v.VacancyId,
			Language:  language.Language,
			Level:     language.Level,
		})
	}

	return &languages, nil
}

func (s *LanguageService) GetAll(ctx context.Context, req *pb.GetAllCLanguagesParams) (*pb.GetAllCLanguagesRes, error) {
	resp, err := s.storage.Language().GetAll(&repo.GetAllLanguagesParams{
		Limit:     req.Limit,
		Page:      req.Page,
		VacancyID: req.VacancyId,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Error("failed to get all language")
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get all language: %v", err)
	}
	response := pb.GetAllCLanguagesRes{
		Languages: make([]*pb.CLanguage, resp.Count),
		Count:     resp.Count,
	}
	for i, v := range resp.Languages {
		response.Languages[i] = parseLanguageModel(v)
	}

	return &response, nil
}

func parseLanguageModel(v *repo.Language) *pb.CLanguage {
	return &pb.CLanguage{
		Id:        v.ID,
		VacancyId: v.VacancyID,
		Language:  v.Language,
		Level:     v.Level,
	}
}
